#include <highlighter.h>

Highlighter::Highlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    keywordFormat.setForeground(QColor(255,85,0));

    const QString keywordPatterns[] = {
        QStringLiteral("\\bholyd\\b"), QStringLiteral("\\bdefine\\b"), QStringLiteral("\\bfinish\\b"),
        QStringLiteral("\\bwrite\\b"), QStringLiteral("\\bread\\b"), QStringLiteral("\\btrue\\b"),
        QStringLiteral("\\bfalse\\b")
    };

    for (const QString &pattern : keywordPatterns) {
        rule.pattern = QRegularExpression(pattern);
        rule.format = keywordFormat;
        highlightingRules.append(rule);
    }

    keywordFormat0.setForeground(QColor(170,0,255));
    keywordFormat0.setFontItalic(true);

    const QString keywordPatterns0[] = {
        QStringLiteral("\\bfor\\b"), QStringLiteral("\\brof\\b"), QStringLiteral("\\bif\\b"),
        QStringLiteral("\\belse\\b"),QStringLiteral("\\belseif\\b"), QStringLiteral("\\bfi\\b"),
        QStringLiteral("\\bwhile\\b"), QStringLiteral("\\belihw\\b")
    };

    for (const QString &pattern0 : keywordPatterns0) {
        rule.pattern = QRegularExpression(pattern0);
        rule.format = keywordFormat0;
        highlightingRules.append(rule);
    }

    const QString keywordPatterns1[]{
    QStringLiteral("\\bChar\\b"), QStringLiteral("\\bFloat\\b"), QStringLiteral("\\bInt\\b"),
    QStringLiteral("\\bString\\b"), QStringLiteral("\\bBool\\b")
    };

    keywordFormat1.setForeground(Qt::blue);

    for (const QString &pattern0 : keywordPatterns1) {
        rule.pattern = QRegularExpression(pattern0);
        rule.format = keywordFormat1;
        highlightingRules.append(rule);
    }

    /*Not implemented cus my language dont have class implementations
    classFormat.setFontWeight(QFont::Bold);
    classFormat.setForeground(Qt::darkMagenta);
    rule.pattern = QRegularExpression(QStringLiteral("\\bQ[A-Za-z]+\\b"));
    rule.format = classFormat;
    highlightingRules.append(rule);
    */

    quotationFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegularExpression(QStringLiteral("\".*\""));
    rule.format = quotationFormat;
    highlightingRules.append(rule);

    /* i dont have function implementation by now
    functionFormat.setFontItalic(true);
    functionFormat.setForeground(Qt::blue);
    rule.pattern = QRegularExpression(QStringLiteral("\\b[A-Za-z0-9_]+(?=\\()"));
    rule.format = functionFormat;
    highlightingRules.append(rule);
    */

    singleLineCommentFormat.setForeground(Qt::gray);
    singleLineCommentFormat.setFontItalic(true);
    rule.pattern = QRegularExpression(QStringLiteral("#[^\n]*"));
    rule.format = singleLineCommentFormat;
    highlightingRules.append(rule);

    multiLineCommentFormat.setForeground(Qt::gray);
    multiLineCommentFormat.setFontItalic(true);
    commentStartExpression = QRegularExpression(QStringLiteral("/\\*"));
    commentEndExpression = QRegularExpression(QStringLiteral("\\*/"));

}

void Highlighter::highlightBlock(const QString &text)
{
    for (const HighlightingRule &rule : qAsConst(highlightingRules)) {
        QRegularExpressionMatchIterator matchIterator = rule.pattern.globalMatch(text);
        while (matchIterator.hasNext()) {
            QRegularExpressionMatch match = matchIterator.next();
            setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
    }
    setCurrentBlockState(0);
    int startIndex = 0;
    if (previousBlockState() != 1)
        startIndex = text.indexOf(commentStartExpression);
    while (startIndex >= 0) {
        QRegularExpressionMatch match = commentEndExpression.match(text, startIndex);
        int endIndex = match.capturedStart();
        int commentLength = 0;
        if (endIndex == -1) {
            setCurrentBlockState(1);
            commentLength = text.length() - startIndex;
        } else {
            commentLength = endIndex - startIndex
                    + match.capturedLength();
        }
        setFormat(startIndex, commentLength, multiLineCommentFormat);
        startIndex = text.indexOf(commentStartExpression, startIndex + commentLength);
    }
}
