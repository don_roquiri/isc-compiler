#include <mainwindow.h>
#include <ui_mainwindow.h>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QDebug>
#include <lexical.h>
#include <synthax.h>
#include <compiler.h>
#include <ui_static_components.h>
#include <QElapsedTimer>
#include <globalcursor.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableLog->setWordWrap(true);
    ui->tableLogError->setWordWrap(true);
    ui->tableLog->resizeColumnsToContents();
    ui->tableLogError->resizeColumnsToContents();
    ui->tableLog->setTextElideMode(Qt::ElideLeft);
    ui->tableLogError->setTextElideMode(Qt::ElideLeft);

    ui->tabedFiles->removeTab(0);
    ui->tabedFiles->setTabsClosable(true);
    createInstanceCodeEditor("Newfile " + QString::number(readedFileTabs.size()));
    nameP = "";

    config_compiler(this);
}

MainWindow::~MainWindow()
{    
    readedFileTabs.clear();
    delete ui;
}


void MainWindow::on_actionOpen_triggered()
{
    QString newFileID = "Newfile " + QString::number(readedFileTabs.size());
    createInstanceCodeEditor(newFileID);
}

void MainWindow::createInstanceCodeEditor(QString id){
    auto newInstance = new  CodeEditor();
    newInstance->labelCursor = ui->labelCursor;
    auto highlighter = new Highlighter();
    auto layout = new QHBoxLayout();
    layout->addWidget(newInstance);
    highlighter->setDocument(newInstance->document());
    auto* newTab = new QTabWidget();
    newTab->setLayout(layout);

    Tabs tab;
    tab.codeEditor = newInstance;
    tab.highligther = highlighter;
    readedFileTabs.append(tab);
    ui->tabedFiles->addTab(newTab, id);
}

void MainWindow::on_actionOpen_2_triggered()
{
    QFile file;
    QTextStream io;
    QString name = QFileDialog::getOpenFileName(this,tr("Abrir ISC Code"),
                                                "/",
                                                "ISC Code (*.isc *.ISC)");
    if(name.isEmpty()){
        return;
    }
    nameP = name;
    file.setFileName(name);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    if(!file.isOpen()){
        QMessageBox::critical(this,"ERROR", file.errorString());
        return;
    }
    io.setDevice(&file);
    createInstanceCodeEditor(name);
    int index = ui->tabedFiles->currentIndex();
    readedFileTabs.value(index).codeEditor->setPlainText(io.readAll());
    io.flush();
    file.flush();
    file.close();
}

void MainWindow::on_actionSave_triggered()
{
    if(nameP.isEmpty()){
        saveAs();
        return;
    }
    QFile file;
    QTextStream io;
    if(nameP.isEmpty()){
        QMessageBox::critical(this,"ERROR","Archivo no válido");
        return;
    }
    if(!nameP.endsWith(".isc")) nameP += ".isc";
    file.setFileName(nameP);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    if(!file.isOpen()){
        QMessageBox::critical(this,"ERROR", file.errorString());
        return;
    }
    io.setDevice(&file);
    int index = ui->tabedFiles->currentIndex();
    io << readedFileTabs.value(index).codeEditor->toPlainText();
    file.close();
}

void MainWindow::saveAs(){
    QFile file;
    QTextStream io;
    QString name = QFileDialog::getSaveFileName(this, tr("Guardar Como"),"/","ISC Code (*.isc *.ISC)");
    if(name.isEmpty()){
        QMessageBox::critical(this,"ERROR","Archivo no válido");
        return;
    }
    if(!name.endsWith(".isc"))name = name += ".isc";
    nameP = name;
    file.setFileName(name);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    if(!file.isOpen()){
        QMessageBox::critical(this,"ERROR", file.errorString());
        return;
    }
    io.setDevice(&file);
    int index = ui->tabedFiles->currentIndex();
    io << readedFileTabs.value(index).codeEditor->toPlainText();
    file.close();
}

void MainWindow::on_actionSave_As_triggered()
{
    saveAs();
}

void MainWindow::on_actionClose_triggered()
{
    int index = ui->tabedFiles->currentIndex();
    readedFileTabs.value(index).codeEditor->clear();
    nameP = "";
}

void MainWindow::on_actionCheck_Synthax_triggered()
{
    optimization_level = config.optimization_level;
    processor = config.processor;

    QElapsedTimer t_start;
    t_start.start();
    int index = ui->tabedFiles->currentIndex();
    QString text = readedFileTabs.value(index).codeEditor->toPlainText();

    static UiStaticComponents ui_components(
                ui->tableTipes,ui->tableOperands,ui->tableOperators,
                ui->tableJumps,ui->tableCuadruplus,ui->tableLog,ui->tableLogError,
                ui->lblExecutionStack
                );

    bool error = false;
    Compiler c(text, optimization_level, &error);

    c.compile();
    int millis_transcured = t_start.elapsed();

    if(!c.synthax->statusSemanthic || !c.synthax->status || !c.lexical->status){
        int index = ui->tabedFiles->currentIndex();
        readedFileTabs.at(index).codeEditor->highlightCurrentLineError(currentRow,currentCol);
    }

    QString status
            = c.lexical->status? "<b><font color=\"green\">Analizis Lexico Ok: " :
                                 "<b><font color=\"red\">Error encontrado en fase Lexico: ";

    status += c.synthax->status? " <b><font color=\"green\">Sintaxis Ok</f1ont></b>" :
                                 " <b><font color=\"red\"> Error en fase de Sintaxis</font></b>";

    status += c.synthax->statusSemanthic? " <b><font color=\"green\"> Semantica Ok</font></b>" :
                                          " <b><font color=\"red\"> Error en fase de Semantica</font></b>";

    status += "\tTiempo transcurrido: " + QString::number(millis_transcured) + "ms.";
    ui->tableLog->resizeColumnsToContents();
    ui->tableLogError->resizeColumnsToContents();
    ui->statusLabel->setText(status);
}

void MainWindow::on_actionAbout_QT_triggered()
{
    QMessageBox::aboutQt(this, "QT Version");
}

void MainWindow::on_actionBuild_triggered()
{
    config.setModal(true);
    if(config.exec() == QDialog::Accepted){
        optimization_level = config.optimization_level;
        processor = config.processor;
        qDebug() << "Update";
    }
}


void MainWindow::on_actionRun_triggered()
{
}

void MainWindow::on_actionAbout_ISC_IDE_triggered()
{
    QString txt;
    txt = "ISC IDE es un entorno de desarrollo para el lenguaje"
          " ISC.\ndesarrollado en el Instituto Tecnologico de Durango"
          " durante la clase d Automas I\n"
          " Alumno: Pablo Eduardo Martinez Solis";
    QMessageBox::about(this,"ISC IDE", txt);
}



void MainWindow::on_tabedFiles_tabCloseRequested(int index)
{
    bool enabled = readedFileTabs.size() >= 1;

    ui->actionCheck_Synthax->setEnabled(enabled);

    QString id = ui->tabedFiles->tabText(index);
    ui->tabedFiles->removeTab(index);
    readedFileTabs.removeAt(index);
}

void MainWindow::on_tabedFiles_currentChanged(int index)
{
    qDebug() << index;
}
