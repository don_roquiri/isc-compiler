#include "config_compiler.h"
#include "ui_config_compiler.h"
#include "thread"
#include "mainwindow.h"
#include "qdebug.h"

config_compiler::config_compiler(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::config_compiler)
{
    ui->setupUi(this);
    processor = std::thread::hardware_concurrency() + 1 - std::thread::hardware_concurrency();
    optimization_level = ui->cbOptimizationLevel->currentText();

    const auto processor_count = std::thread::hardware_concurrency();
    for(unsigned int i = 1; i <= processor_count; i++){
        ui->cbProcessors->addItem(QString::number(i));
    }
}

config_compiler::~config_compiler()
{
    delete ui;
}

void config_compiler::on_buttonBox_rejected()
{

}

void config_compiler::on_pushButton_clicked()
{

}

void config_compiler::on_buttonBox_accepted()
{
    /* *opt =*/
    optimization_level = ui->cbOptimizationLevel->currentText();
    /**processor*/
    processor = ui->cbProcessors->currentText().toUInt();
}


