#include "compiler.h"
#include "token.h"
#include "QtDebug"
#include "QLabel"
#include "ui_static_components.h"

uint8_t Compiler::OPTIMIZATION_LEVEL = 0;



Compiler::Compiler(QString &text, bool *_error){
    //qDebug() << "Constructor";
    if(Compiler::OPTIMIZATION_LEVEL == 0){
        Compiler::OPTIMIZATION_LEVEL = 0;
    }
    error = _error;
    lexical = new  Lexical (text, error);
    synthax = new  Synthax (error);
}

Compiler::Compiler(QString &text, QString &optimization, bool *_error){
    //qDebug() << "Constructor";
    error = _error;

    if(optimization == "O")
        Compiler::OPTIMIZATION_LEVEL = O;
    else if(optimization == "O2")
        Compiler::OPTIMIZATION_LEVEL = O2;
    else if(optimization == "O3")
        Compiler::OPTIMIZATION_LEVEL = O3;
    else
        Compiler::OPTIMIZATION_LEVEL = O;

    lexical = new  Lexical (text, error);
    synthax = new  Synthax (error);

}

Compiler::~Compiler(){
    if(!*error){
        //QString newLine = QString("-").repeated(72);
        //QString newLineS = QString("-").repeated(42);
        QMutableHashIterator iterator(Synthax::cuadruples_hash);

        while(iterator.hasNext()){
            int i;
            auto taked_accordion = iterator.next();
            auto taked_cuadruple = taked_accordion.value();
            QMutableHashIterator symbol_table_iterator (*taked_cuadruple->hash_id);

            //qDebug() << "Class name: " << taked_accordion.key() << newLine;
            //qDebug() << newLineS;
            //qDebug("|%-20s|%-10s|%-10s|\n",
            //       "ID","TIPO","TIPOID");
            //qDebug() << newLineS;

            while(symbol_table_iterator.hasNext()){
                auto taked_symbol = symbol_table_iterator.next();
                bool skipPrint = true;

                if(OPTIMIZATION_LEVEL >= O2){
                    if(!taked_symbol.value().used_var){
                        //symbol_table_iterator.remove();
                        skipPrint = false;
                    }
                }

                if(skipPrint){
                    int rc = UiStaticComponents::tableTipes->rowCount();
                    UiStaticComponents::tableTipes->insertRow(rc);
                    UiStaticComponents::tableTipes->setItem(rc,0,new QTableWidgetItem(qUtf8Printable(taked_symbol.key())));
                    UiStaticComponents::tableTipes->setItem(rc,1,new QTableWidgetItem(qUtf8Printable(taked_symbol.value().value.lexema)));
                    UiStaticComponents::tableTipes->setItem(rc,2,new QTableWidgetItem(QString::number(taked_symbol.value().type)));
                    //qDebug("|%-20s|%-10s|%-10d|",
                    //       qUtf8Printable(taked_symbol.key()),
                    //       qUtf8Printable(taked_symbol.value().value.lexema),
                    //       taked_symbol.value().type
                    //       );
                }
            }


            //qDebug() << newLineS;
            //qDebug() << newLine;
            //qDebug("|%-8s|%-10s|%-15s|%-15s|%-20s|\n",
            //       "Contador","Operador","Operando1","OPerando2","Resultado");
            //qDebug() << newLine;

            for(i = 0; i < taked_cuadruple->size(); i++){
                int rc = UiStaticComponents::tableCuadruples->rowCount();
                UiStaticComponents::tableCuadruples->insertRow(rc);
                UiStaticComponents::tableCuadruples->setItem(rc,0,new QTableWidgetItem(QString::number(i)));
                UiStaticComponents::tableCuadruples->setItem(rc,1,new QTableWidgetItem(qUtf8Printable(taked_cuadruple->at(i).ope.lexema)));
                UiStaticComponents::tableCuadruples->setItem(rc,2,new QTableWidgetItem(qUtf8Printable(taked_cuadruple->at(i).opn0.lexema)));
                UiStaticComponents::tableCuadruples->setItem(rc,3,new QTableWidgetItem(qUtf8Printable(taked_cuadruple->at(i).opn1.lexema)));
                UiStaticComponents::tableCuadruples->setItem(rc,4,new QTableWidgetItem(qUtf8Printable(taked_cuadruple->at(i).res.lexema)));
                //qDebug("|%-8d|%-10s|%-15s|%-15s|%-20s|\n",
                //       i,
                //       qUtf8Printable(taked_cuadruple->at(i).ope),
                //       qUtf8Printable(taked_cuadruple->at(i).opn0),
                //       qUtf8Printable(taked_cuadruple->at(i).opn1),
                //       qUtf8Printable(taked_cuadruple->at(i).res)
                //       );
            }
            //qDebug() << newLine;
            //qDebug() << "Class name: " << taked_accordion.key() << newLine;
            //taked_cuadruple->hash_id->clear();
            //delete taked_cuadruple->hash_id;
            //taked_cuadruple->hash_id = NULL;
        }
    }
    Synthax::cuadruples_hash.clear();
    delete lexical;
    delete synthax;
}


void Compiler::compile(){
    Token *token;
    while ((token = lexical->check()) != nullptr && !*error) {
        //qDebug() << token->lexema << " " << token->gramema;
        synthax->check(*token);
    }
}
