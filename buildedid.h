#pragma once

/**This header help to build identifier and save the real ID *
 * QString id it is used for store the real id               *
 * and the lexem it's used to store all the id example       *
 * id = function                                             *
 * lexem = function(a,b)                                     **/

#include <token.h>

class BuildedId : public Token{
public:
    QString id;
};
