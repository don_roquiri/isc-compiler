#include <lexical.h>
#include <QChar>
#include <QDebug>
#include <QStringBuilder>
#include <globalcursor.h>

QHash<QString, unsigned short int> Lexical::reservedWords;

Lexical::Lexical(QString &_text, bool *_error){

    reservedWords["holyd"] = 1000;
    reservedWords["define"] = 1001;
    reservedWords["if"] = 1003;
    reservedWords["while"] = 1005;
    reservedWords["read"] = 1006;
    reservedWords["write"] = 1007;
    reservedWords["finish"] = 1010;
    reservedWords["elihw"] = 1012;
    reservedWords["elseif"] = 1013;
    reservedWords["else"] = 1014;
    reservedWords["fi"] = 1015;
    reservedWords["for"] = 1004;
    reservedWords["rof"] = 1011;
    reservedWords["Int"] = 1016;
    reservedWords["Float"] = 1017;
    reservedWords["Char"] = 1018;
    reservedWords["String"] = 1019;
    reservedWords["Bool"] = 1020;
    reservedWords["true"] = 1047;
    reservedWords["false"] = 1048;

    error = _error;
    index = 0;
    nError = 0;
    currentRow = 1;
    currentCol = 0;
    text = _text.append("\n\x0");
    containsNumber = false;
    isString = false;
    relatedAgain = false;
    isEqual = false;
    isNotCientifica = false;
    isOpRela = false;
    isOpLog = false;
    isBlockComment = false;
    isDiv = false;
    UiStaticComponents::tableErrors->setRowCount(0);
    UiStaticComponents::tableTokens->setRowCount(0);
}

Token* Lexical::check(){
    token = "";
    edo = 0;
    unsigned short int col = 0;
    QString description;
    QChar car;
    if(hasMoreItems()){
        while(edo <= 31){
            car = readChar();
            col = relateToken(car);
            /*qDebug() << "-------------------------------------------------------------------------";
            qDebug() << "Columna: " << col << endl <<"Old Edo: " << edo << endl << "Caracter: " << car;
            qDebug() << "-------------------------------------------------------------------------";*/
            edo = Lexical::m[edo][col];
            /*qDebug() << "Columna: " << col << endl << "New Edo: " << edo << endl << "Caracter: " << car;
            qDebug() << "-------------------------------------------------------------------------";*/
        }
        //generateMessageToken(edo, token, description);
        data = tokenMsg(edo, token, description);

        containsNumber = false;
        isString = false;
        relatedAgain = false;
        isEqual = false;
        isNotCientifica = false;
        isOpRela = false;
        isOpLog = false;
        isBlockComment = false;
        isDiv = false;
        token = "";
        edo = 0;
        return &data;
    }else{
        return nullptr;
    }
}

inline bool Lexical::generateMessageToken(unsigned short int edo, QString token, QString &description){
    unsigned short int e = edo;
    if(e <= 132){
        int rc = UiStaticComponents::tableTokens->rowCount();
        UiStaticComponents::tableTokens->insertRow(rc);
        data = tokenMsg(e, token, description);
        UiStaticComponents::tableTokens->setItem(rc, 0, new QTableWidgetItem(QString::number(e)));
        UiStaticComponents::tableTokens->setItem(rc, 1, new QTableWidgetItem(data.lexema));
        UiStaticComponents::tableTokens->setItem(rc, 2, new QTableWidgetItem(description));
        return true;
    }else{
        status = true;
        *error = true;
        nError++;
        int rc = UiStaticComponents::tableTokens->rowCount();
        UiStaticComponents::tableErrors->insertRow(rc);
        data = errorMsg(e, token, description);
        qDebug() << "error lexico... " << e << " " << data.lexema;
        description.append(" En la linea: " + QString::number(currentRow) +
                           " Columna: " + QString::number(currentCol));
        UiStaticComponents::tableErrors->setItem(rc, 0, new QTableWidgetItem(QString::number(e)));
        UiStaticComponents::tableErrors->setItem(rc, 1, new QTableWidgetItem(data.lexema));
        UiStaticComponents::tableErrors->setItem(rc, 2, new QTableWidgetItem(description));
        return false;
    }
}

inline QChar Lexical::readChar(){
    if(hasMoreItems()){
        QChar car = text[index++];
        currentCol++;
        return car;
    }else
        return 0;
}

inline bool Lexical::hasMoreItems(){return index < text.size();}

inline unsigned short int Lexical::relateToken(QChar &c){
    if(c == 'E'){
        token.append(c);
        if(containsNumber){
            isNotCientifica = true;
            return 4;
        }else
            return 1;
    }else if(c == 'e'){
        token.append(c);
        if(containsNumber){
            isNotCientifica = true;
            return 5;
        }else{
            return 1;//testing...
        }
    }else if(c >= 'A' && c <= 'Z'){if(isEqual || isOpRela || isDiv) relateAgain(c); else token.append(c); return 0;}
    else if(c >= 'a' && c <= 'z'){if(isEqual || isOpRela || isDiv) relateAgain(c); else token.append(c); return 1;}
    else if(c >= '0' && c <= '9'){
        if(isEqual || isOpRela || isDiv) relateAgain(c); else token.append(c);
        containsNumber = true;
        return 2;
    }else if(c == '_'){if(isEqual || isOpRela) relateAgain(c); else token.append(c); return 3;}
    else if(c == '\''){token.append(c); return 6;}
    else if(c == '"'){
        if(isEqual) relateAgain(c); else token.append(c); isString = true; return 7;
    }else if(c == '#'){token.append(c); isBlockComment = true; return 8;}
    else if(c == '+'){if(isNotCientifica) token.append(c); else relateAgain(c); return 9; }
    else if(c == '-'){if(isNotCientifica) token.append(c); else relateAgain(c); return 10;}
    else if(c == '*'){if(isBlockComment) token.append(c); else relateAgain(c); return 11;}
    else if(c == '/'){
        if(isBlockComment) token.append(c);
        else if(text[index] == '*'){
            token.append(c);
            isBlockComment = true;
        } else{ isDiv = true; relateAgain(c); }
        return 12;
    }else if(c == '%'){relateAgain(c); return 13;}
    else if(c == '='){if(!isEqual && !isOpRela) relateAgain(c);  else token.append(c); isEqual = true; return 14;}
    else if(c == '>'){if(!isEqual) relateAgain(c); else token.append(c); isOpRela = true; return 15;}
    else if(c == '<'){if(!isEqual) relateAgain(c); else token.append(c); isOpRela = true; return 16;}
    else if(c == '!'){if(!isEqual) relateAgain(c); else token.append(c); isOpRela = true; return 17;}
    else if(c == '&'){if(!isEqual && !isOpLog) relateAgain(c); else token.append(c); isOpLog = true; return 18;}
    else if(c == '|'){if(!isEqual && !isOpLog) relateAgain(c); else token.append(c); isOpLog = true; return 19;}
    else if(c == '('){relateAgain(c); return 20;}
    else if(c == ')'){relateAgain(c); return 21;}
    else if(c == '['){relateAgain(c); return 22;}
    else if(c == ']'){relateAgain(c); return 23;}
    else if(c == ':'){relateAgain(c); return 24;}
    else if(c == ';'){relateAgain(c); return 25;}
    else if(c == '.'){if(!containsNumber) relateAgain(c); else token.append(c); return 26;}
    else if (c == ' '){if(isString || isBlockComment) token.append(c); return 29;}
    else if(c == '\n'){
        currentRow++;
        currentCol = 1;
        return 27;
    }else if(c == '\t'){return 28;}
    else if(c == '\b'){return 29;}
    else if (c == ','){relateAgain(c); return 31;}
    else if(c == '\x0'){relateAgain(c); relatedAgain = false; return 32;}
    else if(c == '\xa'){currentCol--; return 33;}
    else{token.append(c); return 30;}//!=
}

inline void Lexical::relateAgain(QChar &c){
    if(token.length() > 0 && !isString && !isBlockComment){
        index--;
        currentCol--;
    }else{
        token.append(c);
    }
}

inline Token Lexical::errorMsg(unsigned short int &e, QString token, QString &description){
    QString message = "Error: "; message.append(QString::number(e));
    data.gramema = e;
    data.lexema = token;
    switch(e){
    case 500: description = "Lexical Error: No Corresponde al lenguaje"; return data;
    case 501: description = "Lexical Error: Numero Flotante esperado"; return data;
    case 502: description = "Lexical Error: Exponencial esperado"; return data;
    case 503: description = "Lexical Error: Numero Real esperado"; return data;
    case 504: description = "Lexical Error: Caracter esperado"; return data;
    case 505: description = "Lexical Error: &:Operación logica incompleta"; return data;
    case 506: description = "Lexical Error: |:Operación logica incompleta"; return data;
    case 507: description = "Lexical Error: Comilla esperada"; return data;
    case 508: description = "Lexical Error: _:No corresponde al lenguaje"; return data;
    case 509: description = "Lexical Error: Identificador mal formado"; return data;
    case 510: description = "Lexical Error: Error se encontro el fin de archivo"; return data;
    default: description = "Lexical Error: Desconocido"; return data;
    }
}

inline Token Lexical::tokenMsg(unsigned short int &e, QString token, QString &description){
    data.gramema = 1000;
    data.lexema = token;
    if(e == 100 || e == 101){
        if(reservedWords.contains(data.lexema)){
            e = 101;
            data.gramema = reservedWords.value(data.lexema);
            return data;
        }
        data.gramema = 1002;
        description = "ID";
        return data;
    } else
        switch(e){
        case 102: data.gramema = 1037; description = "Entero"; return data;
        case 103: data.gramema = 1038; description = "Flotante"; return data;
        case 104: data.gramema = 1039; description = "Not. Cientifica"; return data;
        case 105: data.gramema = 1040; description = "Caracter"; return data;
        case 106: data.gramema = 1041; description = "String"; return data;
        case 107: data.gramema = 0xFFFF; description = "Comentario"; return data;//WIP
        case 108: data.gramema = 1026; description = "Operadores Aritmetico +"; return data;
        case 109: data.gramema = 1027; description = "Operadores Aritmetico -"; return data;
        case 110: data.gramema = 1023; description = "Operadores Aritmetico *"; return data;
        case 111: data.gramema = 1024; description = "Operadores Aritmetico /"; return data;
        case 112: data.gramema = 1025; description = "Operadores Aritmetico %"; return data;
        case 113: data.gramema = 1022; description = "="; return data;
        case 114: data.gramema = 1028; description = "=="; return data;
        case 115: data.gramema = 1045; description = "!"; return data;
        case 116: data.gramema = 1029; description = "!="; return data;
        case 117: data.gramema = 1030; description = "<"; return data;
        case 118: data.gramema = 1031; description = "<="; return data;
        case 119: data.gramema = 1032; description = ">"; return data;
        case 120: data.gramema = 1033; description = ">="; return data;
        case 121: data.gramema = 1034; description = "&&"; return data;
        case 122: data.gramema = 1035; description = "||"; return data;
        case 123: data.gramema = 1009; description = ":"; return data;
        case 124: data.gramema = 0xFFFF; description = "."; return data;//WIP
        case 125: data.gramema = 1046; description = ";"; return data;
        case 126: data.gramema = 1042; description = "("; return data;
        case 127: data.gramema = 1043; description = ")"; return data;
        case 128: data.gramema = 1021; description = "["; return data;
        case 129: data.gramema = 1036; description = "]"; return data;
        case 131: data.gramema = 0xFFFF; description = "Comentario Bloque"; return data;
        case 132: data.gramema = 1008; description = ','; return data;
        case 130: data.gramema = 1044; description = "EOF"; return data;
        default: data.gramema = 0xFFFF; description = "Diferente"; return data;
        }
}

Lexical::~Lexical(){

}
