#pragma once

#include <QString>

class Token
{
public:
    Token(){};
    Token(QString l, unsigned short int g){
        gramema = g;
        lexema = l;
    }
    unsigned short int gramema;
    QString lexema;
};

