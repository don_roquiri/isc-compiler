#include "ui_static_components.h"

QTableWidget* UiStaticComponents::tableJumps;
QTableWidget* UiStaticComponents::tableTipes;
QTableWidget* UiStaticComponents::tableOperands;
QTableWidget* UiStaticComponents::tableOperators;
QTableWidget* UiStaticComponents::tableTokens;
QTableWidget* UiStaticComponents::tableErrors;
QTableWidget* UiStaticComponents::tableCuadruples;
QTextEdit* UiStaticComponents::stackText;

UiStaticComponents::UiStaticComponents(
        QTableWidget *tableTipes, QTableWidget *tableOperands, QTableWidget *tableOperators,
        QTableWidget *tableJumps, QTableWidget *tableCuadruples, QTableWidget *tableTokens,
        QTableWidget *tableErrors, QTextEdit* stackText
        )
{
    this->tableJumps=tableJumps;
    this->tableTipes=tableTipes;
    this->tableOperands=tableOperands;
    this->tableOperators=tableOperators;
    this->tableErrors=tableErrors;
    this->tableCuadruples=tableCuadruples;
    this->tableTokens=tableTokens;
    this->stackText=stackText;
}
