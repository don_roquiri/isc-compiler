#pragma once

/**This class would help to store lenght of index of array or table      *
 * also we can use for store the length of args and data type,           *
 * i use the var type to know if it's an array or function statement     *
 * type = 0 for vectors                                                  *
 * type = 1 for function args                                            *
 * count var it's a global var used to count index of the array         **/

#include <QList>
#include <QDebug>

inline int counter;

class IndexArgsLength : public QList<int>{
public:
    IndexArgsLength(){counter = 0;}
    void addArgs(int gramem){append(gramem);}
    void addIndex(int size){append(size);}
    bool isNullPointer(){return counter >= size();}
    bool compareSize(int size){return size < at(counter++);}
    bool compareDataType(unsigned short int gramem){return at(counter++) == gramem;}
    bool needsMoreElements(){return counter < size();}
    void resetCounter(){counter = 0;}

    QString getDiff(){
        QString res;
        int diff = counter - size();
        diff *= ((diff < 0) * (-1) + (diff > 0));
        res += QString::number(diff) + " elementos";
        return res;
    }

};
