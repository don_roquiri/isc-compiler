#ifndef CONFIG_COMPILER_H
#define CONFIG_COMPILER_H

#include <QDialog>

namespace Ui {
class config_compiler;
}

class config_compiler : public QDialog
{
    Q_OBJECT

public:
    QString optimization_level;
    QString path_main;
    unsigned int processor;

    explicit config_compiler(QWidget *parent = nullptr);
    ~config_compiler();

private slots:
    void on_buttonBox_rejected();

    void on_pushButton_clicked();

    void on_buttonBox_accepted();

private:
    Ui::config_compiler *ui;
};

#endif // CONFIG_COMPILER_H
