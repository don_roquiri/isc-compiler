#pragma once
#include <row_cuadruples.h>
#include <QStack>
#include <QHash>

class Optimization
{
    QVector<RowCuadruple> optimizedCuadruples;
    QHash<QString, Token> hashOfExpressions;
    int startIndexToReplace, endIndexToReplace;

public:
    Token pivot;
    void relate(RowCuadruple token);
    bool hasOptimization;
    Optimization();
    ~Optimization();
    void booleanStatement(RowCuadruple &token);
    void arithmeticStatement(RowCuadruple &token);
    void reset();
    QVector<RowCuadruple> getOptimizedCuadruples();
    QString karatsubaAlgorithm(QString x, QString y);
};

