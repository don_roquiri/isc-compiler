#pragma once

#include <QTableWidget>
#include "QTextEdit"

class UiStaticComponents
{
public:

    static QTableWidget *tableTipes;
    static QTableWidget *tableOperands;
    static QTableWidget *tableOperators;
    static QTableWidget *tableJumps;
    static QTableWidget *tableCuadruples;
    static QTableWidget *tableTokens;
    static QTableWidget *tableErrors;
    static QTextEdit *stackText;

    UiStaticComponents(
            QTableWidget *tableTipes,
            QTableWidget *tableOperands,
            QTableWidget *tableOperators,
            QTableWidget *tableJumps,
            QTableWidget *tableCuadruples,
            QTableWidget *tableTokens,
            QTableWidget *tableErrors,
            QTextEdit *stackText
            );
};

