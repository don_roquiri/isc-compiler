#pragma once
#include "QHash"
#include "token.h"
#include "cuadruple_mdl.h"
/**This is used to store cuadruples of classes. *
 * Also it's used for linkage purpose           */
class AccordionCuadruples: public QHash<QString, CuadrupleMdl*>{};
