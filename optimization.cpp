#include "optimization.h"
#include "qmath.h"
#include "qdebug.h"

void Optimization::relate(RowCuadruple token)
{
    QString key = token.ope.lexema % token.opn0.lexema % token.opn1.lexema;
    qDebug() << key;
    auto finded = hashOfExpressions.find(key);
    hasOptimization = finded != hashOfExpressions.end();
    if(hasOptimization){
        pivot = finded.value();
    }else{
        hashOfExpressions.insert(key, token.res);
    }
}

Optimization::Optimization()
{
}

Optimization::~Optimization()
{
    hashOfExpressions.clear();
}

void Optimization::reset()
{
    hashOfExpressions.clear();
}

QVector<RowCuadruple> Optimization::getOptimizedCuadruples(){
    return optimizedCuadruples;
}

QString Optimization::karatsubaAlgorithm(QString x, QString y){
    if(x.size() <= 2 && y.size() <= 2){
        hasOptimization = false;
        return "";
    }

    int m = qMin(x.size(),y.size());
    int m2 = qFloor(m / 2);

    QString a = x.left(m2);
    QString b = x.mid(m2);
    QString c = y.left(m2);
    QString d = y.left(m2);

    QString z0 = karatsubaAlgorithm(b,d);
    QString z1 = karatsubaAlgorithm((a + b),(c + d));
    QString z2 = karatsubaAlgorithm(a,c);
    RowCuadruple row;

    hasOptimization = true;
    return "";
}
