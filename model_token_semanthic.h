#pragma once

#define VAR 0
#define FUNCTION 1
#define ARRAY 2
#define USER_DATA 3

#include <token.h>

class ModelTokenSemanthic{
public:
    Token id;
    int8_t type;
    Token value;
    bool used_var;
};
