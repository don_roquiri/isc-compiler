#pragma once

#include "lexical.h"
#include "synthax.h"
#include "QString"
#include "QTableWidget"
#include "QLabel"
#include "QTextEdit"
#include <QStack>
#define O 1
#define O2 2
#define O3 3
class Compiler
{
private:
    void printStack(QStack<Token> &stack);
    void printStack(QStack<unsigned int> &stack);
public:
    bool *error;
    static uint8_t OPTIMIZATION_LEVEL;
    Lexical *lexical;
    Synthax *synthax;
    Compiler(QString &text,bool *error);
    ~Compiler();
    void compile();
    Compiler(QString &text, QString &optimization, bool *_error);
};
