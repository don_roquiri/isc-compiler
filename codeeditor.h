#pragma once

#include <QPlainTextEdit>
#include <QTextBlock>
#include <QPainter>
#include <QLabel>

class CodeEditor : public QPlainTextEdit
{
    Q_OBJECT

public:
    CodeEditor(QWidget *parent = nullptr);
    QLabel *labelCursor;
    void lineNumberAreaPaintEvent(QPaintEvent *event);
    int lineNumberAreaWidth();    
    void highlightCurrentLineError(int row, int col);
protected:
    void resizeEvent(QResizeEvent *event) override;

private slots:
    void updateLineNumberAreaWidth(int newBlockCount);
    void highlightCurrentLine();
    void updateLineNumberArea(const QRect &rect, int dy);
    void updateLabelCoords();

private:
    QWidget *lineNumberArea;
};

