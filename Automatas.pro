QT       += core gui

#QMAKE_CXXFLAGS_RELEASE -= -O2
#QMAKE_CXXFLAGS_RELEASE += -O3
#QMAKE_LFLAGS_RELEASE -= -O2

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17 -std=c++17

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
#This Macro is used to tell to the compiler if we need debug code
#or not
#DEFINES += TEST
DEFINES *= QT_USE_QSTRINGBUILDER
# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    codeeditor.cpp \
    compiler.cpp \
    config_compiler.cpp \
    highlighter.cpp \
    lexical.cpp \
    main.cpp \
    mainwindow.cpp \
    optimization.cpp \
    synthax.cpp \
    ui_static_components.cpp

HEADERS += \
    accordion_cuadruple.h \
    buildedid.h \
    codeeditor.h \
    compiler.h \
    config_compiler.h \
    cuadruple_mdl.h \
    globalcursor.h \
    highlighter.h \
    index_args_length.h \
    lexical.h \
    linenumberarea.h \
    mainwindow.h \
    model_token_semanthic.h \
    optimization.h \
    row_cuadruples.h \
    synthax.h \
    token.h \
    ui_static_components.h

FORMS += \
    config_compiler.ui \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc
