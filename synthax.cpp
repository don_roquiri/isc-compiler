#include "synthax.h"
#include "QtDebug"
#include "token.h"
#include <globalcursor.h>
#include <compiler.h>

bool Synthax::status = true;
QHash<unsigned int, unsigned short int> Synthax::hash_row_semanthic;
QHash<unsigned short int, QString> Synthax::hash_index_synthax;
QHash<unsigned short int, Token> Synthax::hash_operator;
AccordionCuadruples Synthax::cuadruples_hash;

Synthax::Synthax(bool *_error){

    UiStaticComponents::tableTipes->setRowCount(0);
    UiStaticComponents::tableOperands->setColumnCount(0);
    UiStaticComponents::tableOperators->setColumnCount(0);
    UiStaticComponents::tableOperands->setRowCount(0);
    UiStaticComponents::tableOperators->setRowCount(0);

    UiStaticComponents::tableJumps->setColumnCount(0);
    UiStaticComponents::tableJumps->setRowCount(0);
    UiStaticComponents::tableCuadruples->setRowCount(0);

    UiStaticComponents::tableJumps->insertRow(UiStaticComponents::tableJumps->rowCount());
    UiStaticComponents::tableOperands->insertRow(UiStaticComponents::tableOperands->rowCount());
    UiStaticComponents::tableOperators->insertRow(UiStaticComponents::tableOperators->rowCount());
    UiStaticComponents::stackText->clear();

    error = _error;
    Synthax::status = true;

    executionStack.append(1044);
    executionStack.append(0);

    /**Creationg keys using mathematic aproach                       *
     * bijective NxN -> N                                            *
     * pi(k1,k2) = ((k1 + k2) * (k1 + k2 + 1) / 2) + k2              *
     * Benefits of this approach                                     *
     * 1.- As others have made clear, if you plan to implement a     *
     * pairing function, you may soon find you need arbitrarily      *
     * large integers (bignums).                                     *
     * 2.- If you don't want to make a distinction between the pairs *
     * (a, b) and (b, a), then sort a and b before applying the      *
     *  pairing function.                                            *
     * 3.- Actually I lied. You are looking for a bijective ZxZ -> N *
     * mapping. Cantor's function only works on non-negative numbers.*
     *  This is not a problem however, because it's easy to define   *
     *  a bijection f : Z -> N, like so:                             *
     * f(n) = n * 2 if n >= 0                                        *
     * f(n) = -n * 2 - 1 if n < 0                                   **/

    hash_row_semanthic[bijectiveKeyCalc(INT, INT)] = 0;
    hash_row_semanthic[bijectiveKeyCalc(INT, 0)] = 0;
    hash_row_semanthic[bijectiveKeyCalc(CTE_INT, 0)] = 0;
    hash_row_semanthic[bijectiveKeyCalc(INT, CTE_INT)] = 0;
    hash_row_semanthic[bijectiveKeyCalc(INT, FLOAT)] = 1;
    hash_row_semanthic[bijectiveKeyCalc(INT, CTE_FLOAT)] = 1;
    hash_row_semanthic[bijectiveKeyCalc(FLOAT, FLOAT)] = 2;
    hash_row_semanthic[bijectiveKeyCalc(FLOAT, 0)] = 2;
    hash_row_semanthic[bijectiveKeyCalc(CTE_FLOAT, 0)] = 2;
    hash_row_semanthic[bijectiveKeyCalc(FLOAT, CTE_FLOAT)] = 2;
    hash_row_semanthic[bijectiveKeyCalc(CHAR, CHAR)] = 3;
    hash_row_semanthic[bijectiveKeyCalc(CHAR, 0)] = 3;
    hash_row_semanthic[bijectiveKeyCalc(CTE_CHAR, 0)] = 3;
    hash_row_semanthic[bijectiveKeyCalc(CHAR, CTE_CHAR)] = 3;
    hash_row_semanthic[bijectiveKeyCalc(CHAR, STR)] = 4;
    hash_row_semanthic[bijectiveKeyCalc(CHAR, CTE_STRING)] = 4;
    hash_row_semanthic[bijectiveKeyCalc(STR, STR)] = 5;
    hash_row_semanthic[bijectiveKeyCalc(STR, 0)] = 5;
    hash_row_semanthic[bijectiveKeyCalc(CTE_STRING, 0)] = 5;
    hash_row_semanthic[bijectiveKeyCalc(BOOL, BOOL)] = 6;
    hash_row_semanthic[bijectiveKeyCalc(BOOL, 0)] = 6;
    hash_row_semanthic[bijectiveKeyCalc(UNK, UNK)] = 7;
    hash_row_semanthic[bijectiveKeyCalc(UNK, 0)] = 7;

    /**Indexs of grammars*/
    hash_index_synthax[1000] = "holyd ";
    hash_index_synthax[1001] = "define ";
    hash_index_synthax[1002] = "id ";
    hash_index_synthax[1003] = "if ";
    hash_index_synthax[1004] = "for ";
    hash_index_synthax[1005] = "while ";
    hash_index_synthax[1006] = "read ";
    hash_index_synthax[1007] = "write ";
    hash_index_synthax[1008] = ", ";
    hash_index_synthax[1009] = ": ";
    hash_index_synthax[1010] = "finish ";
    hash_index_synthax[1011] = "rof ";
    hash_index_synthax[1012] = "elihw ";
    hash_index_synthax[1013] = "elseif ";
    hash_index_synthax[1014] = "else ";
    hash_index_synthax[1015] = "fi ";
    hash_index_synthax[1016] = "Int ";
    hash_index_synthax[1017] = "Float ";
    hash_index_synthax[1018] = "Char ";
    hash_index_synthax[1019] = "String ";
    hash_index_synthax[1020] = "Bool ";
    hash_index_synthax[1021] = "[ ";
    hash_index_synthax[1022] = "= ";
    hash_index_synthax[1023] = "* ";
    hash_index_synthax[1024] = "/ ";
    hash_index_synthax[1025] = "% ";
    hash_index_synthax[1026] = "+ ";
    hash_index_synthax[1027] = "- ";
    hash_index_synthax[1028] = "== ";
    hash_index_synthax[1029] = "!= ";
    hash_index_synthax[1030] = "< ";
    hash_index_synthax[1031] = "<= ";
    hash_index_synthax[1032] = "> ";
    hash_index_synthax[1033] = ">= ";
    hash_index_synthax[1034] = "&& ";
    hash_index_synthax[1035] = "|| ";
    hash_index_synthax[1036] = "] ";
    hash_index_synthax[1037] = "cteentera ";
    hash_index_synthax[1038] = "ctereal ";
    hash_index_synthax[1039] = "ctenotacion ";
    hash_index_synthax[1040] = "ctecaracter ";
    hash_index_synthax[1041] = "ctestring ";
    hash_index_synthax[1042] = "( ";
    hash_index_synthax[1043] = ") ";
    hash_index_synthax[1044] = "$ ";
    hash_index_synthax[1045] = "! ";
    hash_index_synthax[1046] = "; ";
    //Indices de no terminales!
    hash_index_synthax[0] = "PROGRAM ";
    hash_index_synthax[1] = "PROGRAM2 ";
    hash_index_synthax[2] = "PROGRAM3 ";
    hash_index_synthax[3] = "ESTATUTOS ";
    hash_index_synthax[4] = "ESTATUTOS2 ";
    hash_index_synthax[5] = "OESTATUTOS2 ";
    hash_index_synthax[6] = "ESTATUTOS3 ";
    hash_index_synthax[7] = "DECLARA ";
    hash_index_synthax[8] = "DECLARA2 ";
    hash_index_synthax[9] = "RID_DIM ";
    hash_index_synthax[10] = "RID_DIM2 ";
    hash_index_synthax[11] = "ID_DIM ";
    hash_index_synthax[12] = "ID_DIM2 ";
    hash_index_synthax[13] = "DIM ";
    hash_index_synthax[14] = "DIM2 ";
    hash_index_synthax[15] = "EST_ASIG ";
    hash_index_synthax[16] = "ASIG ";
    hash_index_synthax[17] = "ASIG2 ";
    hash_index_synthax[18] = "DIM_ASIG ";
    hash_index_synthax[19] = "EXPR_AUX ";
    hash_index_synthax[20] = "EXPR_AUX2 ";
    hash_index_synthax[21] = "EXPR ";
    hash_index_synthax[22] = "OEXPR2 ";
    hash_index_synthax[23] = "EXPR2 ";
    hash_index_synthax[24] = "OEXPR3 ";
    hash_index_synthax[25] = "EXPR3 ";
    hash_index_synthax[26] = "OEXPR3-1 ";
    hash_index_synthax[27] = "EXPR4 ";
    hash_index_synthax[28] = "OEXPR4 ";
    hash_index_synthax[29] = "EXPR5 ";
    hash_index_synthax[30] = "OEXPR5 ";
    hash_index_synthax[31] = "TERM ";
    hash_index_synthax[32] = "OTERM ";
    hash_index_synthax[33] = "FACT ";
    hash_index_synthax[34] = "EST_IF ";
    hash_index_synthax[35] = "EST_IF2 ";
    hash_index_synthax[36] = "EST_ELSEIF ";
    hash_index_synthax[37] = "EST_ELSE ";
    hash_index_synthax[38] = "EST_WHILE ";
    hash_index_synthax[39] = "EST_READ ";
    hash_index_synthax[40] = "ID_R ";
    hash_index_synthax[41] = "ID_R2 ";
    hash_index_synthax[42] = "EST_WRITE ";
    hash_index_synthax[43] = "EST_FOR ";
    hash_index_synthax[44] = "OPREL ";
    hash_index_synthax[45] = "TIPO ";

    /**Index operator*/
    hash_operator[MUL] = Token("MUL",0);
    hash_operator[SUM] = Token("SUM",0);
    hash_operator[SUB] = Token("SUB",0);
    hash_operator[DIV] = Token("DIV",1);
    hash_operator[MOD] = Token("RCALL __DIV_MOD",2);
    hash_operator[LE] = Token("CMP",3);
    hash_operator[GE] = Token("CMP",3);
    hash_operator[GEQ] = Token("CMP",3);
    hash_operator[LEQ] = Token("CMP",3);
    hash_operator[EQ] = Token("CMP",3);
    hash_operator[NEQ] = Token("CMP",3);
    hash_operator[LOR] = Token("OR",4);
    hash_operator[LAND] = Token("AND",4);
    hash_operator[NOT] = Token("NOT",4);
    hash_operator[ASSIGMENT] = Token("MOV",5);
    hash_operator[WRITE] = Token("RCALL __write",6);
    hash_operator[READ] = Token("RCALL __read",6);
    //hash_operator["<<"] = Token("SHR",0);
    //hash_operator[">>"] = Token("SHL",0);
    //hash_operator["^"] = Token("XOR",0);
    //hash_operator["&"] = Token("XOR",0);
    //hash_operator["|"] = Token("XOR",0);

}

void Synthax::check(Token token){
    axis = 0;
    col = 0;
    currentStackV = 0;
    x = true;
    if(token.gramema != 0xFFFF){
#ifdef TEST
        buildStack();
#endif
        while (x) {
            currentStackV = executionStack.top();//3
            if(currentStackV <= 45){//3.5no terminal
                col = token.gramema - 1000;
                //renglon y columna
                //qDebug() << "Valor Stack: " << currentStackV << " Valor gramema " << token.gramema << " Valor lexeman " << token.lexema;
                //qDebug() << "Renglon: " << currentStackV << " Col: " << col;
                axis = Synthax::m[currentStackV][col];//4
                //qDebug() << "Axis: "<< axis;
                if (axis >= 700){
                    //Error de sintaxis
                    QString description;
                    generateMessageToken(axis,token.lexema,description);
                    goto QUIT;
                }else{
                    executionStack.pop();
#ifdef TEST
                    buildStack();
                    atatchProductionsToStack(axis);
                    buildStack();
#else
                    atatchProductionsToStack(axis);
#endif
                    if(popLambda){
                        executionStack.pop();
                        popLambda = false;
                    }
                }
            }else {
                //qDebug() << "Comparar";
                //qDebug() << "Valor Stack: " << currentStackV << " Valor gramema " << token.gramema << " Valor lexeman " << token.lexema;
                if(token.gramema != currentStackV){
                    if(currentStackV >= 2000){
                        //Semantica
                        analysisSemanthic(token, currentStackV);
                        executionStack.pop();
                    }else{
                        //Error de sintaxis no match
                        QString description;
                        generateMessageToken(753,token.lexema,description);
                        goto QUIT;
                    }
                } else {
                    //match
                    x = false;
                    executionStack.pop();
                }
            }
        }
    }
    return;
QUIT:
    Synthax::status = false;
    return;
}

inline void Synthax::atatchProductionsToStack(unsigned short int &axis){
    switch (axis) {
    case 1: executionStack.append({1010 ,1 ,1009 ,1002 ,2020 ,1000 }); break;//holyd 2020 id2 : PROGRAM2 finish
    case 2: executionStack.append({2 }); break;
    case 3: executionStack.append({1 ,7 }); break;
    case 4: executionStack.append({1 ,3 }); break;
    case 5: executionStack.append({1044 }); popLambda = true; break;
    case 6: executionStack.append({6 ,4 }); break;
    case 7: executionStack.append({1046 ,5 }); break;
    case 8: executionStack.append({1044 }); popLambda = true; break;
    case 9: executionStack.append({2010 ,15 }); break;//accion2010 EST_ASIG
    case 10: executionStack.append({34 }); break;
    case 11: executionStack.append({38 }); break;
    case 12: executionStack.append({39 }); break;
    case 13: executionStack.append({42 }); break;
    case 14: executionStack.append({43 }); break;
    case 15: executionStack.append({3 }); break;
    case 16: executionStack.append({1044 }); popLambda = true; break;
    case 17: executionStack.append({8 }); break;
    case 18: executionStack.append({7 ,1046 ,2005 ,45 ,1009 ,9 ,1001, 2041}); break;//accion41 define RID_DIM : TIPO accion5 ; DECLARA
    case 19: executionStack.append({1044 }); popLambda = true; break;
    case 20: executionStack.append({10 ,11 }); break;//ID_DIM RID_DIM2
    case 21: executionStack.append({9 ,1008 }); break;//, RID_DIM
    case 22: executionStack.append({1044 }); popLambda = true; break;
    case 23: executionStack.append({12 ,1002 ,2000}); break;//accion0 id ID_DIM2
    case 24: executionStack.append({2024, 13 }); break;//DIM accion24
    case 25: executionStack.append({1044 }); popLambda = true; break;
    case 26: executionStack.append({14 ,1036 ,1037 ,2023 ,1021 ,2031}); break;//accion31 [ accion23 cteentera ] DIM2
    case 27: executionStack.append({13 }); break;
    case 28: executionStack.append({1044 }); popLambda = true; break;
    case 29: executionStack.append({21, 1022, 2003, 16}); break;//accion5 ASIG accion10 = accion3 accion2 EXPR
    case 30: executionStack.append({2002, 17 ,1002 ,2021}); break;//accion21 id ASIG2(debe construirse todo el id primero) a[0,2]
    case 31: executionStack.append({18 }); break;
    case 32: executionStack.append({1044 }); popLambda = true; break;
    case 33: executionStack.append({1036 ,2022 ,19 ,1021, 2021, 2031}); break;//accion31 accion21 [ EXPR_AUX accion22 ]
    case 34: executionStack.append({20 ,21 }); break;
    case 35: executionStack.append({19 ,1008, 2030 }); break;//accion 30 , EXPR_AUX
    case 36: executionStack.append({1044 }); popLambda = true; break;
    case 37: executionStack.append({22 ,23 }); break;
    case 38: executionStack.append({21 ,1035, 2036 }); break;//accion36 || EXPR
    case 39: executionStack.append({1044 }); popLambda = true; break;
    case 40: executionStack.append({24 ,25 }); break;
    case 41: executionStack.append({23 ,1034, 2035}); break;//accion35 && EXPR2
    case 42: executionStack.append({1044 }); popLambda = true; break;
    case 43: executionStack.append({26 }); break;
    case 44: executionStack.append({27 ,1045 ,2003}); break;//accion5 ! EXPR4
    case 45: executionStack.append({27 }); break;
    case 46: executionStack.append({28 ,29 }); break;
    case 47: executionStack.append({29 ,44 }); break;
    case 48: executionStack.append({1044 }); popLambda = true; break;
    case 49: executionStack.append({30 , 31 }); break; //TERM OEXPR5
    case 50: executionStack.append({29 ,1026 ,2006}); break;//accion6 + EXPR5
    case 51: executionStack.append({29 ,1027 ,2006}); break;//accion6 - EXPR5
    case 52: executionStack.append({1044 }); popLambda = true; break;
    case 53: executionStack.append({32 ,33 }); break;//FACT OTERM
    case 54: executionStack.append({31 ,1023, 2004}); break;//OTERM * accion4
    case 55: executionStack.append({31 ,1024, 2004}); break;//OTERM / accion4
    case 56: executionStack.append({31 ,1025, 2004}); break;//OTERM % accion4
    case 57: executionStack.append({1044 }); popLambda = true; break;
    case 58: executionStack.append({16}); break;
    case 59: executionStack.append({1037 ,2002}); break;//cteentera accion 2002
    case 60: executionStack.append({1038 ,2002}); break;//ctereal accion 2002
    case 61: executionStack.append({1040 ,2002}); break;//ctecaracter accion 2002
    case 62: executionStack.append({1041 ,2002}); break;//ctestring 2002
    case 63: executionStack.append({1039 ,2002}); break;//cternotacion accion 2002
    case 64: executionStack.append({1043 ,2008 ,21 ,1042 , 2007}); break;//accion7 ( EXPR accion8 )
    case 65: executionStack.append({1015 ,2040 ,35 ,2029 ,1 ,1009 ,2028 ,2037 ,21 ,1003 }); break;//if EXPR accion37 accion28 : PROGRAM2 accion29 EST_IF2 accion40 fi
    case 66: executionStack.append({2039 ,37 ,36 }); break;//EST_ELSEIF EST_ELSE accion39
    case 67: executionStack.append({35 ,2029 ,1 ,1009 ,2028 ,2037 ,21 ,1013 }); break;//elseif EXPR accion37 accion28 : PROGRAM2 accion29 EST_IF2
    case 68: executionStack.append({1044 }); popLambda = true; break;
    case 69: executionStack.append({1 ,1009 ,1014 }); break;//else : PROGRAM2
    case 70: executionStack.append({1044 }); popLambda = true; break;
    case 71: executionStack.append({1012 ,2027, 1 ,1009 ,2028 ,2037 ,21 ,2038 ,1005 }); break;//while accion38 EXPR accion37 accion28 : PROGRAM2 accion27 elihw
    case 72: executionStack.append({1043 ,2033 ,40 ,1042 ,1006 ,2032}); break;//accion32 read ( EXPR_AUX accion33 )
    case 73: executionStack.append({41 ,1002 ,2002, 2021}); break;//accion21 accion2 id ID_R2
    case 74: executionStack.append({40 ,1008 }); break;
    case 75: executionStack.append({1044 }); popLambda = true; break;
    case 76: executionStack.append({1043 ,2033 ,19 ,1042 ,1007 ,2032}); break;//accion32 write ( EXPR_AUX accion33 )
    case 77: executionStack.append({1011 ,2027 ,1 ,1009 ,2026 ,15 ,1046 ,2037 ,21 ,2025 ,1046 ,15 ,1004 }); break;//for EST_ASIG ; accion25 EXPR accion37 ; EST_ASIG accion26 : PROGRAM2 accion27 rof
    case 78: executionStack.append({1028 ,2034}); break;//Accion 20034 ==
    case 79: executionStack.append({1029 ,2034}); break;//Accion 20034 !=
    case 80: executionStack.append({1031 ,2034}); break;//Accion 20034 <=
    case 81: executionStack.append({1033 ,2034}); break;//Accion 20034 >=
    case 82: executionStack.append({1032 ,2034}); break;//Accion 20034 >
    case 83: executionStack.append({1030 ,2034}); break;//Accion 20034 <
    case 84: executionStack.append({1016 ,2001}); break;//Acción 1 Int
    case 85: executionStack.append({1017 ,2001}); break;//Acción 1 Float
    case 86: executionStack.append({1018 ,2001}); break;//Acción 1 Char
    case 87: executionStack.append({1019 ,2001}); break;//Acción 1 String
    case 88: executionStack.append({1020 ,2001}); break;//Acción 1 Bool
    }
}

inline void Synthax::analysisSemanthic(Token &token, int action){
    switch(action){
    /**Just add the data type to the identifiers, we create a buffer that stores the id of the define statement *
      * Example define a,b,c : Int; buffer contains {a,b,c} and until we find ':' we add the                    *
      * next token (dataType)                                                                                  **/
    case 2000:{addTokenToMTS(token);}break;
        /**Now dequeue the buffer of ids and asign the data type*/
    case 2001:{
        while (!buffer_id_todo.isEmpty()) {
            auto model = buffer_id_todo.dequeue();
            model.value = token;//Added Type example list {a,b,c} as Int
            hash_id[model.id.lexema] =  model;
        }
    }break;
        /**This is a little bit tricky because if we are building the identifier, we must need to know it *
         * because if it is a cte we must just insert the value tokenToSymbolTable contains                        *
         * the real identifier, so if we found the id on hash args we compare the length of the array to  *
         * to see if it is not an array null pointer exception                                            *
         * also we check if the index its allowed                                                         *
         * define a[100] : Int; a[101] = 100 this is an error                                            **/
    case 2002:{
        if(!writeOrReadStatement){
            if(token.gramema >= 1037 && token.gramema <= 1041){
                if(buildingId){//If it's building the id concat the lexem
                    tokenToSymbolTable.value.lexema += token.lexema;
                    if(itsArray || itsFunction){
                        if(!checkArgsLength(token)){return;}
                    }
                    return;
                }else{//If isn't building just add
                    tokenToSymbolTable.id = token;
                    addCol();
                }
            }else{
                /**We assume that is a id, so we need find his data type on the table of symbols*
                 * and check if the id is contained in the table of symbols                     */
                addCol();
            }
        }else{
            if(!itsArray && !itsFunction){
                if(token.gramema >= 1037 && token.gramema <= 1041){
                    tokenToSymbolTable.id = token;
                    addCol();
                }
            }else{
                if(token.gramema >= 1037 && token.gramema <= 1041){
                    tokenToSymbolTable.value.lexema += token.lexema;
                    if(!checkArgsLength(token)){return;}
                }
            }
        }
    }break;
    case 2003:{
        addColOperator(token);
    }break;
    case 2004:{
        if(stackOperator.isEmpty()){
            addColOperator(token);
            return;
        }
        if (stackOperator.top().gramema == SUM ||
                stackOperator.top().gramema == SUB ||
                /*stackOperator.top().gramema == LOR ||*/
                stackOperator.top().gramema == EQU ||
                stackOperator.top().gramema == NOT ||
                stackOperator.top().gramema == LPARE)
            addColOperator(token);
        else {
            postfix(token);
            addColOperator(token);
        }
    }break;
    case 2005:
        defineStatement = false;
        break;
    case 2006:{
        if(stackOperator.isEmpty()){
            addColOperator(token);
            return;
        }
        if (stackOperator.top().gramema == SUM ||
                stackOperator.top().gramema == SUB /*||stackOperator.top().gramema == LOR */){
            postfix(token);
        }else{
            while ((stackOperator.top().gramema == MUL ||
                    stackOperator.top().gramema == DIV ||/*||stackOperator.top().gramema == LAND*/
                    stackOperator.top().gramema == MOD)
                   && !stackOperands.isEmpty()) {
                postfix(token);
            }
        }
        addColOperator(token);
    }break;
        /**Added MFF ( Left  **/
    case 2007:{
        addColOperator(token);
    }break;
    case 2008:{
        while(stackOperator.top().gramema != LPARE){
            if(stackOperator.top().gramema == NOT)
                postfixForNotShiftOp();
            else
                postfix(token);
        }
        stackOperator.pop();
    }break;
    case 2009: { stackOperator.pop(); } break;
    case 2010:{
        while (stackOperator.top().gramema != EQU && !stackOperator.isEmpty()) {
            if(stackOperator.top().gramema == NOT)
                postfixForNotShiftOp();
            else
                postfix(token);
        }
        optimization.reset();
        postfixAsigmentStatement();
    }break;
        /**Added name on class to id list and change type to userdata *
         * And also add cuadrup table to the hash table               */
    case 2020:{
        ModelTokenSemanthic mdl;
        mdl.id = token;
        mdl.type = USER_DATA;
        mdl.value.lexema = "USER_DATA";
        mdl.value.gramema = 889;
        mdl.used_var = true;
        cuadruples.hash_id = &hash_id;
        Synthax::cuadruples_hash.insert(token.lexema, &cuadruples);
        hash_id.insert(token.lexema, mdl);
    }break;
        /**Build the identifier to lately be added on the hash table (Table Semanthic Type) *
         * if it's building just concat the lexemm of the token                             *
         * we asume that every id it's a var until it goes to another production           **/
    case 2021:{
        if(buildingId){
            tokenToSymbolTable.value.lexema += token.lexema;
        }else{
            tokenToSymbolTable.type = VAR;
            tokenToSymbolTable.id = token;
            tokenToSymbolTable.value = token;
            buildingId = true;
        }
    }break;
        /**This performs the action after ] (close square bracket) *
         * reset the counter for elements to 0, we save the id of  *
         * token because it's deleted before                      **/
    case 2022:{
        buildingId = itsArray = false;
        tokenToSymbolTable.value.lexema += token.lexema;
    }break;
        /**We store the index of the array or the args of the function *
         * Example fn function(define a,b : Int, define c : Float)     *
         * in  the buffer index it's stored two Ints and one Float     *
         * gramem or if it's an array store the index                **/
    case 2023:{
        itsArray = true;
        buffer_index_todo.enqueue(token.lexema.toInt());
    }break;
        /**This case it's the action 2024 it's useful because detect if we are using a functions   *
         * or arrays to store the length of args (with the data type) or index and in case it's an *
         * array we store the  MAX_VALUE (0xFFFFFFFF) available for array, if buffer_index(args)   *
         * it's not empty we store the data type                                                  **/
    case 2024:{
        IndexArgsLength length_args_index;
        while(!buffer_index_todo.isEmpty()){
            length_args_index.append(buffer_index_todo.dequeue());
        }
        //length_args_index.type = !itsFunction? 0 : 1;
        hash_args_length.insert(
                    buffer_id_todo.last().id.lexema,
                    length_args_index
                    );
        //qDebug() << "Reset counter!";
        args.resetCounter();
    }break;
        //for
    case 2025:{
        while (stackOperator.top().gramema != EQU && !stackOperator.isEmpty()) {
            postfix(token);
        }
        optimization.reset();
        postfixAsigmentStatement();
        addColJumps();
    }break;
        //for
    case 2026:{
        while (stackOperator.top().gramema != EQU && !stackOperator.isEmpty()) {
            postfix(token);
        }
        optimization.reset();
        postfixAsigmentStatement();
        addColJumps();
        Token opn1 = stackOperands.pop();
        RowCuadruple row_cuadruple;
        row_cuadruple.ope.lexema = "SF";
        row_cuadruple.opn0.lexema = opn1.lexema;
        cuadruples.append(row_cuadruple);
    }break;
        /***SI for the FOR_STATEMENT*/
    case 2027:{
        RowCuadruple row_cuadruple;
        cuadruples[stackJumps.pop()].res.lexema = QString::number(cuadruples.size() + 1);
        row_cuadruple.ope.lexema = "SI";
        row_cuadruple.res.lexema = QString::number(stackJumps.pop());
        cuadruples.append(row_cuadruple);
    }break;
        /**SF for the WHILE and IF statement*/
    case 2028:{
        addColJumps();
        Token opn = stackOperands.pop();
        RowCuadruple row_cuadruple;
        row_cuadruple.ope.lexema = "SF";
        row_cuadruple.opn0.lexema = opn.lexema;
        cuadruples.append(row_cuadruple);
    }break;
        /**SI for the IF_STATEMENT*/
    case 2029:{
        int aux_jump = stackJumps.pop();
        addColJumps();
        RowCuadruple row_cuadruple;
        cuadruples[aux_jump].res.lexema = QString::number(cuadruples.size() + 1);
        row_cuadruple.ope.lexema = "SI";
        cuadruples.append(row_cuadruple);
    }break;
    case 2030:{
        if(itsArray){
            if(buildingId){
                tokenToSymbolTable.value.lexema += token.lexema;
            }else{
                tokenToSymbolTable.id = token;
                tokenToSymbolTable.value = token;
                buildingId = true;
            }
        }else{
            addCol();
            postfixForWriteReadStatement();
        }
    }break;
    case 2031:{
        itsArray = true;
        if(defineStatement){
            buffer_id_todo.back().type = ARRAY;
            //defineStatement = false;
        }else{
           tokenToSymbolTable.type = ARRAY;
       }
    }break;
    case 2032:{
        addColOperator(token);
        writeOrReadStatement = true;
    }break;
    case 2033:{
        writeOrReadStatement = false;
        addCol();
        postfixForWriteReadStatement();
        stackOperator.pop();//Pop Write or Read Operator
    }break;
        /**If its an relation operator turn on the flag!*/
        /*I need to work here but is not necesary rightnow**/
    case 2034:{
        addColOperator(token);
        itsOperatorRelational = true;
    }break;
    case 2035:{
        if(!stackOperator.isEmpty()){
            if ( stackOperator.top().gramema != LOR )
                postfix(token);
        }
        addColOperator(token);
    }break;
    case 2036:{
        if(!stackOperator.isEmpty()){
            if ( stackOperator.top().gramema == LOR ){
                postfix(token);
            }else{
                while (( stackOperator.top().gramema == LAND )
                       && !stackOperands.isEmpty()
                       && !*error) {
                    postfix(token);
                }
            }
        }
        addColOperator(token);
    }break;
        /**This means that we are on IF, FOR, WHILE statement and we need to pop all the elements*/
    case 2037:{
        while(!stackOperator.isEmpty() && !*error){
            if(stackOperator.top().gramema == NOT)
                postfixForNotShiftOp();
            else
                postfix(token);
        }
        optimization.reset();
    }break;
        /**Action 2025 and 2038 are similar, but this is used for IF and WHILE statements*/
    case 2038:{addColJumps();}break;
        /**Last semanthic action for if statment before fi*/
    case 2039:{
        ifStatement = true;
        cuadruples[stackJumps.pop()].res.lexema = QString::number(cuadruples.size());
    }break;
    case 2040:{
        if(!ifStatement){
            cuadruples[stackJumps.pop()].res.lexema = QString::number(cuadruples.size());
        }else{
            ifStatement = false;
        }
    }break;
    case 2041:{
        defineStatement = true;
        break;
    }
    }

}


void Synthax::postfixAsigmentStatement(){
    //Generamos cuadruplo tambien
    Token op = stackOperator.pop();
    Token opn0 = stackOperands.pop();
    Token opn1 = stackOperands.pop();

    auto col = relateSemanthic(&op);
    auto ren = relateSemanthic(&opn0, &opn1);
    auto edo = Synthax::m_semanthic[ren][col];
    //if(col == 5 && ren == 1)
    //    edo = FLOAT;
    /*else*/ if(edo >= I_I && edo <= UNK){
        QString description;
        generateMessageToken(edo, "Error entre tipos! " , description);
        return;
    }
    RowCuadruple row_cuadruple;
    row_cuadruple.ope = op;
    row_cuadruple.opn0 = opn1;
    row_cuadruple.res = opn0;
    cuadruples.append(row_cuadruple);
}

void Synthax::postfixForWriteReadStatement(){
    //Generamos cuadruplo tambien
    Token op = stackOperator.top();
    Token opn0 = stackOperands.pop();
    unsigned short int _opn0 = opn0.gramema;

    if(_opn0 == ID){
        _opn0 = hash_id.value(tokenToSymbolTable.id.lexema).value.gramema;
    }

    int r =  hash_row_semanthic.value(bijectiveKeyCalc(opn0.gramema, 0));
    int c = hash_operator[op.gramema].gramema;
    int edo = Synthax::m_semanthic[r][c];

    if(edo >= 800 && edo <= 808){
        QString description;
        generateMessageToken(edo, "Error entre tipos! " , description);
    }else{
        RowCuadruple row;
        row.ope = op;
        row.res = opn0;
        cuadruples.append(row);
    }
}

void Synthax::postfixForNotShiftOp(){

    //Generamos cuadruplo tambien
    Token op = stackOperator.pop();
    Token opn0 = stackOperands.pop();
    unsigned short int _opn0 = opn0.gramema;

    if(_opn0 == ID){
        _opn0 = hash_id.value(tokenToSymbolTable.id.lexema).value.gramema;
    }

    int r =  hash_row_semanthic.value(bijectiveKeyCalc(opn0.gramema, 0));
    int c = hash_operator[op.gramema].gramema;
    int edo = Synthax::m_semanthic[r][c];

    if(edo >= 800 && edo <= 808){
        QString description;
        generateMessageToken(edo, "Error entre tipos! " , description);
    }else{
        Token temp;
        temp.lexema = "R" + QString::number(++tempIndex);
        temp.gramema = edo;
        stackOperands.push(temp);
        RowCuadruple row;
        row.ope = op;
        row.opn0 = opn0;
        row.res = temp;
        cuadruples.append(row);
        int columnCount = UiStaticComponents::tableOperands->columnCount();
        UiStaticComponents::tableOperands->insertColumn(columnCount);
        UiStaticComponents::tableOperands->setItem(0,  columnCount, new QTableWidgetItem(temp.lexema));
    }

}

void Synthax::postfix(Token token){
    //Generamos cuadruplo tambien
    Token op = stackOperator.pop();
    Token opn0 = stackOperands.pop();
    Token opn1 = stackOperands.pop();

    unsigned short int col = relateSemanthic(&op);
    unsigned short int ren = relateSemanthic(&opn0, &opn1);
    unsigned short int edo = Synthax::m_semanthic[ren][col];
    //qDebug() << "Assigment statement";
    qDebug() << edo << " " << col << " " << ren;

    if(edo >= 800 && edo <= 808){
        QString description;
        generateMessageToken(edo, token.lexema, description);
    }else{
        Token temp;
        temp.lexema = "R" + QString::number(++tempIndex);
        temp.gramema = edo;
        stackOperands.push(temp);
        int columnCount = UiStaticComponents::tableOperands->columnCount();
        UiStaticComponents::tableOperands->insertColumn(columnCount);
        UiStaticComponents::tableOperands->setItem(0,  columnCount, new QTableWidgetItem(temp.lexema));
        addCruadrupTable(op, opn1, opn0, temp);
    }

}

ModelTokenSemanthic* Synthax::checkIfIDExist(){
    auto finded = hash_id.find(tokenToSymbolTable.id.lexema);
    if(finded == hash_id.end()){
        QString description;
        generateMessageToken(810, tokenToSymbolTable.id.lexema, description);
        return nullptr;
    }
    finded.value().used_var = true;
    return &finded.value();
}

bool Synthax::checkArgsLength(Token &token){
    auto finded = hash_args_length.find(tokenToSymbolTable.id.lexema);
    if(finded != hash_args_length.end()){
        args = finded.value();
        if(!args.isNullPointer()){
            if(!args.compareSize(token.lexema.toInt())){
                QString description;
                generateMessageToken(812,tokenToSymbolTable.id.lexema, description);
                return false;
            }
        }else{
            QString description;
            generateMessageToken(813,tokenToSymbolTable.id.lexema, description);
            return false;
        }
        return true;
    }
    return false;
}

bool Synthax::checkArgsNeedsMoreElements(){
    auto finded = hash_args_length.find(tokenToSymbolTable.id.lexema);
    args = finded.value();
    return args.needsMoreElements();
}

void Synthax::addColJumps(){
    unsigned int c = cuadruples.size();
    stackJumps.push(c);
    int columnCount = UiStaticComponents::tableJumps->columnCount();
    UiStaticComponents::tableJumps->insertColumn(columnCount);
    UiStaticComponents::tableJumps->setItem(0,  columnCount, new QTableWidgetItem(QString::number(c)));
}

/**We need to ensure that we are processing is valid token:*
 * if it's a ID check if its on the symbol table and next  *
 * check the type, we have also different types that are   *
 * defined in model_token_semantic                         *
 * if the id it's an array check the length elements       *
 * if the id it's a functon check the length of args       **/
void Synthax::addCol(){
    itsArray = itsFunction = buildingId = false;
    if(tokenToSymbolTable.id.gramema == ID){
        auto t_pointer = checkIfIDExist();
        if(t_pointer == nullptr) return;
        t_pointer->used_var = true;
        switch (t_pointer->type) {
        case ARRAY:
        case FUNCTION:{
            if(!checkArgsNeedsMoreElements()){
                args.resetCounter();
                stackOperands.push(tokenToSymbolTable.value);
            }else{
                QString description;
                generateMessageToken(814, tokenToSymbolTable.value.lexema, description);
                //return;
            }
            int columnCount = UiStaticComponents::tableOperands->columnCount();
            UiStaticComponents::tableOperands->insertColumn(columnCount);
            UiStaticComponents::tableOperands->setItem(0,  columnCount, new QTableWidgetItem(tokenToSymbolTable.value.lexema));
            if(itsOperatorRelational){
                postfix(tokenToSymbolTable.value);
                itsOperatorRelational = false;
            }
            return;
        }break;
        }
    }

    stackOperands.push(tokenToSymbolTable.id);
    int columnCount = UiStaticComponents::tableOperands->columnCount();
    UiStaticComponents::tableOperands->insertColumn(columnCount);
    UiStaticComponents::tableOperands->setItem(0,  columnCount, new QTableWidgetItem(tokenToSymbolTable.id.lexema));
    if(itsOperatorRelational){
        postfix(tokenToSymbolTable.id);
        itsOperatorRelational = false;
    }
}



void Synthax::addColOperator(Token &token){
    int columnCount = UiStaticComponents::tableOperators->columnCount();
    UiStaticComponents::tableOperators->insertColumn(columnCount);
    UiStaticComponents::tableOperators->setItem(0, columnCount, new QTableWidgetItem(token.lexema));
    stackOperator.push(token);
}

void Synthax::addCruadrupTable(Token &p_operator, Token &p_operand1, Token &p_operand2, Token &p_res){
    RowCuadruple row_cuadruple;
    row_cuadruple.ope = p_operator;
    row_cuadruple.opn0 = p_operand1;
    row_cuadruple.opn1 = p_operand2;
    row_cuadruple.res = p_res;
    if(Compiler::OPTIMIZATION_LEVEL == O3){
        optimization.relate(row_cuadruple);
        if(optimization.hasOptimization){
            stackOperands.pop();//Sacamos el R generado
            tempIndex--;
            stackOperands.push(optimization.pivot);
            return;
        }
    }
    cuadruples.append(row_cuadruple);
}

inline unsigned short int Synthax::relateSemanthic(Token* _opn0, Token* _opn1){
    unsigned short int op0 = _opn0->gramema;
    unsigned short int op1 = _opn1->gramema;
    //If it's a var, we take a look into the hash to know his type
    //define a:Int; a = 100; we search 'a' and get the type gramem
    if(op0 == ID){
        op0 = hash_id.value(_opn0->lexema).value.gramema;
    }
    if(op1 == ID){
        op1 = hash_id.value(_opn1->lexema).value.gramema;
    }
    //qDebug() << "relate semanthic";
    //qDebug() << op0 << "  " << op1;
    return hash_row_semanthic.value(bijectiveKeyCalc(op0, op1));
}

inline unsigned short int Synthax::relateSemanthic(Token* _op0){
    unsigned short int on0;
    Token ntoken = hash_operator.value(_op0->gramema);
    on0 = ntoken.gramema;
    return on0;
}

void Synthax::generateMessageToken(unsigned short int edo, QString token, QString &description){
    *error = true;
    x = false;
    Synthax::status = false;
    Token data = errorMsg(edo, token, description);
    int row = appendTable(UiStaticComponents::tableErrors);
    description.append(" Se esperaba un: " + relateGrammar(currentStackV));
    description.append(" En la linea: " + QString::number(currentRow) +
                       " Columna: " + QString::number(currentCol));
    UiStaticComponents::tableErrors->setItem(row, 0, new QTableWidgetItem(QString::number(edo)));
    UiStaticComponents::tableErrors->setItem(row, 1, new QTableWidgetItem(data.lexema));
    UiStaticComponents::tableErrors->setItem(row, 2, new QTableWidgetItem(description));
}


inline Token Synthax::errorMsg(unsigned short int e, QString token, QString &description){
    Token data;
    data.gramema = e;
    data.lexema = token;
    switch(e){
    case 700: description = "Synthax Error: Expresion mal formada"; return data;
    case 701: description = "Synthax Error: Expresion mal formada"; return data;
    case 702: description = "Synthax Error: Expresion mal formada"; return data;
    case 703: description = "Synthax Error: Expresion mal formada"; return data;
    case 704: description = "Synthax Error: Expresion mal formada"; return data;
    case 705: description = "Synthax Error: Expresion mal formada"; return data;
    case 706: description = "Synthax Error: Expresion mal formada"; return data;
    case 707: description = "Synthax Error: Expresion mal formada"; return data;
    case 708: description = "Synthax Error: Expresion mal formada"; return data;
    case 709: description = "Synthax Error: Expresion mal formada"; return data;
    case 710: description = "Synthax Error: Expresion mal formada"; return data;
    case 711: description = "Synthax Error: Expresion mal formada"; return data;
    case 712: description = "Synthax Error: Expresion mal formada"; return data;
    case 713: description = "Synthax Error: Expresion mal formada"; return data;
    case 714: description = "Synthax Error: Expresion mal formada"; return data;
    case 715: description = "Synthax Error: Expresion mal formada"; return data;
    case 716: description = "Synthax Error: Expresion mal formada"; return data;
    case 717: description = "Synthax Error: Expresion mal formada"; return data;
    case 718: description = "Synthax Error: Expresion mal formada"; return data;
    case 719: description = "Synthax Error: Expresion mal formada"; return data;
    case 720: description = "Synthax Error: Expresion mal formada"; return data;
    case 721: description = "Synthax Error: Expresion mal formada"; return data;
    case 722: description = "Synthax Error: Expresion mal formada"; return data;
    case 723: description = "Synthax Error: Expresion mal formada"; return data;
    case 724: description = "Synthax Error: Expresion mal formada"; return data;
    case 725: description = "Synthax Error: Expresion mal formada"; return data;
    case 726: description = "Synthax Error: Expresion mal formada"; return data;
    case 727: description = "Synthax Error: Expresion mal formada"; return data;
    case 728: description = "Synthax Error: Expresion mal formada"; return data;
    case 729: description = "Synthax Error: Expresion mal formada"; return data;
    case 730: description = "Synthax Error: Expresion mal formada"; return data;
    case 731: description = "Synthax Error: Expresion mal formada"; return data;
    case 732: description = "Synthax Error: Expresion mal formada"; return data;
    case 733: description = "Synthax Error: Expresion mal formada"; return data;
    case 734: description = "Synthax Error: Expresion mal formada"; return data;
    case 735: description = "Synthax Error: Expresion mal formada"; return data;
    case 736: description = "Synthax Error: Expresion mal formada"; return data;
    case 737: description = "Synthax Error: Expresion mal formada"; return data;
    case 738: description = "Synthax Error: Expresion mal formada"; return data;
    case 739: description = "Synthax Error: Expresion mal formada"; return data;
    case 740: description = "Synthax Error: Expresion mal formada"; return data;
    case 741: description = "Synthax Error: Expresion mal formada"; return data;
    case 742: description = "Synthax Error: Expresion mal formada"; return data;
    case 743: description = "Synthax Error: Expresion mal formada"; return data;
    case 744: description = "Synthax Error: Expresion mal formada"; return data;
    case 745: description = "Synthax Error: Expresion mal formada"; return data;
    case 746: description = "Synthax Error: Expresion mal formada"; return data;
    case 747: description = "Synthax Error: Expresion mal formada"; return data;
    case 748: description = "Synthax Error: Expresion mal formada"; return data;
    case 749: description = "Synthax Error: Expresion mal formada"; return data;
    case 750: description = "Synthax Error: Expresion mal formada"; return data;
    case 751: description = "Synthax Error: Expresion mal formada"; return data;
    case 752: description = "Synthax Error: Expresion mal formada"; return data;
    case 753: description = "Synthax Error: No Match "; return data;
    case 800: {description = "Semanthic Error: I_I"; return data;}
    case 801: {description = "Semanthic Error: I_R"; return data;}
    case 802: {description = "Semanthic Error: R_I"; return data;}
    case 803: {description = "Semanthic Error: R_R"; return data;}
    case 804: {description = "Semanthic Error: C_C"; return data;}
    case 805: {description = "Semanthic Error: C_S"; return data;}
    case 806: {description = "Semanthic Error: S_C"; return data;}
    case 807: {description = "Semanthic Error: S_S"; return data;}
    case 808: {description = "Semanthic Error: B_B"; return data;}
    case 809: {description = "Semanthic Error: Indentificador repetido"; return data;}
    case 810: {description = "Semanthic Error: Variable No Definida"; return data;}
    case 811: {description = "Semanthic Error: Desconocido"; return data;}
    case 812: {description = "Semanthic Error: Indice supera el limite del arreglo "; return data;}
    case 813: {description = "Semanthic Error: La función ó arreglo tiene más elementos de los permitidos " % args.getDiff() % " Mas"; return data;}
    case 814: {description = "Semanthic Error: La función ó arreglo necesita " % args.getDiff() % " Mas"; return data;}
    default: description =  "Synthax Error: Desconocido"; return data;
    }
}

int Synthax::appendTable(QTableWidget *table){
    int row = table->rowCount();
    table->insertRow(row);
    return row;
}

inline void Synthax::addTokenToMTS(Token &p_token){
    if(hash_id.contains(p_token.lexema)){
        QString description;
        generateMessageToken(809, p_token.lexema, description);
    }else{
        ModelTokenSemanthic mdlTkn;
        mdlTkn.id = p_token;
        mdlTkn.type = 0;
        mdlTkn.used_var = false;
        buffer_id_todo.append(mdlTkn);
        hash_id.insert(p_token.lexema, mdlTkn);
    }
}

static QString stack;
inline void Synthax::buildStack(){
    //for(int i = 0; i< executionStack.size(); i++){
    //    stack += relateGrammar(executionStack.at(i));
    //}
    //stack += "\n";
    //UiStaticComponents::stackText->setText(stack);
}

constexpr unsigned int Synthax::bijectiveKeyCalc(unsigned short int k1, unsigned short int k2){
    return k1 + k2;
}

QString Synthax::relateGrammar(unsigned short int g){
    if(g > 2000){
        return "AS:" % QString::number(g) % " ";
    }else{
        return Synthax::hash_index_synthax.value(g);
    }
}

Synthax::~Synthax(){}
