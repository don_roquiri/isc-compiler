import edu.duke.cs.jflap.grammar.GrammarChecker;
import edu.duke.cs.jflap.gui.grammar.GrammarTable;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class FisrtFollows {

    String EMPTY_DERIVATION = "ε";

    FisrtFollows() {

    }

    int rangeTerminalMin, rangeTerminalMax, rangeNonTerminalMin, rangeNonTerminalMax;

    LinkedHashMap<String, Right> rules = new LinkedHashMap<>();
    LinkedHashMap<String, Integer> symbols = new LinkedHashMap<>();

    public void calcFirstFollows(int rowMin0, int rowMax0, int colMin0, int colMax0,
                          int rowMin1, int rowMax1, int colMin1, int colMax1, String glc, String xls) throws IOException {

        getSymbols(symbols, xls, rowMin0, rowMax0, colMin0, colMax0, rowMin1, rowMax1, colMin1, colMax1);


        var iterator = Files.lines(Paths.get(glc)).iterator();
        while (iterator.hasNext()) {
            var line = iterator.next();
            var tokens = line.split(" ");
            if (isNonTerminal(symbols.get(tokens[0]))) {
                JOptionPane.showMessageDialog(null, "Error", "La GLC es inválida error 1", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (!tokens[1].equals("->")) {
                JOptionPane.showMessageDialog(null, "Error", "La GLC es inválida error 2", JOptionPane.ERROR_MESSAGE);
                return;
            }
            ArrayList<Token> derived = new ArrayList<>();
            for (int i = 2; i < tokens.length; i++) {
                derived.add(new Token(tokens[i], Integer.valueOf(symbols.get(tokens[i]))));
            }

            rules.put(tokens[0], new Right(derived));
        }
        calcFirstSet();
    }

    LinkedHashMap<String, Right> createEmptySet() {
        LinkedHashMap<String, Right> set = new LinkedHashMap<>();
        rules.forEach((s, value) -> set.put(s, new Right()));
        return set;
    }


    void calcFirstSet() {
        var firstSet = createEmptySet();
        Right set = null;
        boolean isSetChanged = false;

        do {
            String nonTerminal = "";
            for (Map.Entry<String, Right> entry : rules.entrySet()) {
                String left = entry.getKey();
                nonTerminal = left;
                 set = new Right(firstSet.get(nonTerminal));
                Right right = entry.getValue();
                for (int i = 0; i < right.size(); i++) {
                    var element = right.get(i);
                    for (Token token : element) {
                        if (isTerminal(token.gramem)) {
                            set.add(right.get(i));
                        } else if (isNonTerminal(token.gramem)) {
                            set.addAll(firstSet.get(token.lexem));

                            if (firstSet.get(token.lexem).contains(EMPTY_DERIVATION)) {
                                if (right.get(i + 1) != null) {
                                    set.add(element);
                                }
                            }
                        } else {
                            set.add(new ArrayList<>());
                        }
                    }

                }
            }
            if(firstSet.get(nonTerminal).size() != set.size()){
                firstSet.get(nonTerminal).addAll(set);
                isSetChanged = true;
            }
        }while(isSetChanged);

        firstSet.forEach((s, tokens) -> {
                    System.out.print("FIRST( " + s + " ) = { ");
                    //tokens.forEach(token -> System.out.print(token.lexem + " "));
                    System.out.println("}");
                }
        );
        }


    ArrayList<Token> compact(ArrayList<Token> arr1) {
        return (ArrayList<Token>) arr1.stream().distinct().collect(Collectors.toList());
    }

    ArrayList<Token> uniq(ArrayList<Token> arr1) {
        return (ArrayList<Token>) arr1.stream().filter(token -> symbols.get(token.lexem) == token.gramem);
    }

    ArrayList<Token> union(ArrayList arr1, ArrayList arr2) {
        arr1.addAll(arr2);
        return arr1;
    }

    boolean isTerminal(int value) {
        return value >= rangeTerminalMin && value <= rangeTerminalMax;
    }

    boolean isNonTerminal(int value) {
        return value >= rangeTerminalMin && value <= rangeTerminalMax;
    }

    void getSymbols(LinkedHashMap symbols, String path, int rowMin0,
                    int rowMax0, int colMin0, int colMax0,
                    int rowMin1, int rowMax1, int colMin1,
                    int colMax1) {
        try {
            var f = new File(path);
            var workbook = WorkbookFactory.create(f);
            var sheet = workbook.getSheetAt(0);
            var dataFormatter = new DataFormatter();

            //Non terminal symbols
            String value = "0";
            String key = "0";

            for (int i = rowMin0; i <= rowMax0; i++) {
                key = dataFormatter.formatCellValue(sheet.getRow(colMin0).getCell(i));
                value = dataFormatter.formatCellValue(sheet.getRow(colMax0).getCell(i));
                symbols.put(key, value);
            }

            //Terminal Symbols
            for (int j = colMin1; j <= colMax1; j++) {
                key = dataFormatter.formatCellValue(sheet.getRow(rowMin1).getCell(j));
                value = dataFormatter.formatCellValue(sheet.getRow(rowMax1).getCell(j));
                symbols.put(key, value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Right extends ArrayList<ArrayList<Token>>{
    Right(ArrayList<Token> right){
        add(right);
    }
    Right(){}
    Right(Right right){
        super(right);
    }
}


class Token {
    public String lexem;
    public int gramem;

    public Token(String lexem, int gramem) {
        this.lexem = lexem;
        this.gramem = gramem;
    }
}
