import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class MainForm extends JFrame {

    String xls, glc;

    MainForm() {
        super("Tool");
        renMin1.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        renMax1.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        renMin0.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        renMax0.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        colMax1.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        colMin1.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        colMin0.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        colMax0.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        add(panel1);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        setSize(new Dimension(400, 400));
        spinnerValorError.setEnabled(false);
        header.setEnabled(false);

        btnSeleccionarXls.addActionListener(actionEvent -> {
            var a = new FileDialog(this);
            a.setFilenameFilter((File dir, String name) -> name.toLowerCase().endsWith("xls"));
            a.setMultipleMode(false);
            a.setTitle("Ingrese su XLS!");
            a.setAlwaysOnTop(true);
            a.setVisible(true);
            xls = a.getDirectory() + a.getFile();
            txtXls.setText(xls);
        });

        btnSeleccionarGLC.addActionListener(actionEvent -> {
            var a = new FileDialog(this);
            a.setFilenameFilter((File dir, String name) -> name.toLowerCase().endsWith("txt"));
            a.setMultipleMode(false);
            a.setTitle("Ingrese su GLC!");
            a.setAlwaysOnTop(true);
            a.setVisible(true);
            glc = a.getDirectory() + a.getFile();
            txtGramatica.setText(glc);
        });

        cbOpciones.addItemListener(itemEvent -> {
            int o = cbOpciones.getSelectedIndex();
            System.out.println(o);
            spinnerValorError.setEnabled(true);
            renMax0.setEnabled(true);
            renMin0.setEnabled(true);
            renMax1.setEnabled(true);
            renMin1.setEnabled(true);
            colMax0.setEnabled(true);
            colMin0.setEnabled(true);
            colMax1.setEnabled(true);
            colMin1.setEnabled(true);
            btnSeleccionarGLC.setEnabled(true);
            btnSeleccionarXls.setEnabled(true);
            cbHeader.setEnabled(true);
            cbHeader.setSelected(true);
            header.setEnabled(true);
            switch (o) {
                case 0:
                    spinnerValorError.setEnabled(false);
                    cbHeader.setEnabled(false);
                    header.setEnabled(false);
                    break;
                case 1:
                    btnSeleccionarGLC.setEnabled(false);
                    break;
                case 2:
                    renMax1.setEnabled(false);
                    renMin1.setEnabled(false);
                    colMin1.setEnabled(false);
                    colMax1.setEnabled(false);
                    colMin0.setEnabled(false);
                    colMax0.setEnabled(false);
                    header.setEnabled(false);
                    sp_max_lenght.setEnabled(false);
                    btnSeleccionarXls.setEnabled(false);
                    btnSeleccionarGLC.setEnabled(false);
                    break;
                case 3:
                case 4:
                    break;
            }
        });
        generarButton.addActionListener(actionEvent -> {
            textResul.setText("");
            int o = cbOpciones.getSelectedIndex();
            switch (o) {
                case 0:
                    textResul.setText(
                            generateP((int) renMin0.getValue() - 1, (int) renMax0.getValue() - 1,
                                    (int) colMin0.getValue() - 1, (int) colMax0.getValue() - 1,
                                    (int) renMin1.getValue() - 1, (int) renMax1.getValue() - 1,
                                    (int) colMin1.getValue() - 1, (int) colMax1.getValue() - 1,
                                    glc)
                    );
                    break;
                case 1: {
                    int length;
                    int p_l = (int) sp_max_lenght.getValue();
                    length = p_l == 0 ? 3 : p_l;
                    int header = -1;
                    if (cbHeader.isSelected()) {
                        header = (int) this.header.getValue();
                    }
                    int error = (int) this.spinnerValorError.getValue() == 0 ? 500 : (int) spinnerValorError.getValue();
                    textResul.setText(
                            generateM((int) renMin0.getValue() - 1, (int) renMax0.getValue() - 1,
                                    (int) colMin0.getValue() - 1, (int) colMax0.getValue() - 1,
                                    header - 1, length, error)
                    );
                }
                break;
                case 2: {
                    int p_e = (int) spinnerValorError.getValue();
                    int error = p_e == 0 ? 500 : p_e;
                    textResul.setText(
                            generateS((int) renMin0.getValue(), (int) renMax0.getValue(),
                                    error)
                    );
                }
                break;
                case 3: {
                    fromCodeToXLS(glc);
                }
                break;
                case 4:
                    var firstFollows = new FisrtFollows();
                    try {
                        firstFollows.calcFirstFollows(
                                (int) renMin0.getValue() - 1, (int) renMax0.getValue() - 1,
                                (int) colMin0.getValue() - 1, (int) colMax0.getValue() - 1,
                                (int) renMin1.getValue() - 1, (int) renMax1.getValue() - 1,
                                (int) colMin1.getValue() - 1, (int) colMax1.getValue() - 1,
                                glc, xls);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        });
        cbHeader.addActionListener(actionEvent -> header.setEnabled(cbHeader.isSelected()));
        this.setState(JFrame.MAXIMIZED_BOTH);
        loadTableButton.addActionListener(actionEvent -> {
            try {
                loadExcel();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });


        table1.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    }


    void loadExcel() throws IOException {
        if (xls == null)
            return;
        var f = new File(xls);
        var workbook = WorkbookFactory.create(f);
        var sheet = workbook.getSheetAt(0);

        var dataRows = new Vector<Vector>();
        var header = new Vector<>();

        for (int i = 1; i < sheet.getLastRowNum(); i++) {
            var row = sheet.getRow(i);
            var dataCells = new Vector<>();
            for (int j = 0; j < row.getLastCellNum(); j++) {
                var cell = row.getCell(j);
                if (cell != null) {
                    switch (cell.getCellType()) {
                        case NUMERIC:
                            dataCells.add(String.valueOf(cell.getNumericCellValue()));
                            break;
                        case STRING:
                            dataCells.add(cell.getStringCellValue());
                            break;
                        case BOOLEAN:
                            dataCells.add(String.valueOf(cell.getBooleanCellValue()));
                            break;
                        case FORMULA:
                            dataCells.add(cell.getCellFormula());
                            break;
                        default:
                            dataCells.add(" ");
                            break;
                    }
                }else{
                    dataCells.add(" ");
                }
            }
            if(i == 1) {
                header = dataCells;
            }
            dataRows.add(dataCells);
        }

        workbook.close();

        var mdl = new DefaultTableModel(dataRows, header);
        System.out.println("do you crash?");
        table1.setModel(mdl);
    }


    void getSymbols(HashMap symbols, String path, int rowMin0,
                    int rowMax0, int colMin0, int colMax0,
                    int rowMin1, int rowMax1, int colMin1,
                    int colMax1) {
        try {
            loadExcel();
            var f = new File(xls);
            var workbook = WorkbookFactory.create(f);
            var sheet = workbook.getSheetAt(0);
            var dataFormatter = new DataFormatter();


            //Non terminal symbols
            String value = "0";
            String key = "0";

            for (int i = rowMin0; i <= rowMax0; i++) {
                key = dataFormatter.formatCellValue(sheet.getRow(colMin0).getCell(i));
                value = dataFormatter.formatCellValue(sheet.getRow(colMax0).getCell(i));
                symbols.put(key, value);
            }

            //Terminal Symbols
            for (int j = colMin1; j <= colMax1; j++) {
                key = dataFormatter.formatCellValue(sheet.getRow(rowMin1).getCell(j));
                value = dataFormatter.formatCellValue(sheet.getRow(rowMax1).getCell(j));
                symbols.put(key, value);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    String generateM(int rowMin0, int rowMax0,
                     int colMin0, int colMax0,
                     int header, int max_length,
                     int error) {

        try {
            var f = new File(xls);
            System.out.println(f.exists());
            var workbook = WorkbookFactory.create(f);
            var sheet = workbook.getSheetAt(0);

            StringBuilder toReturn =
                    new StringBuilder(String.format("unsigned short int m[%d][%d]{\n", rowMax0 - rowMin0 + 1, colMax0 - colMin0 + 1));
            var dataFormatter = new DataFormatter();
            if (header != -1) {
                toReturn.append("\t/*");
                for (int i = colMin0; i <= colMax0; i++) {
                    var cellValue = dataFormatter.
                            formatCellValue(
                                    sheet.getRow(header).getCell(i)
                            );
                    toReturn.append(String.format("|%s%s|", cellValue, " ".repeat(Math.abs(cellValue.length() - max_length))));
                }
                toReturn.append("*/\n");
            }

            int count = 0;
            for (int i = rowMin0; i <= rowMax0; i++) {
                toReturn.append("/*").append(count++).append("*/").append("\t{");
                for (int j = colMin0; j <= colMax0; j++) {
                    String cellValue = dataFormatter.
                            formatCellValue(
                                    sheet.getRow(i).getCell(j)
                            );
                    var repeat = " ".repeat(Math.abs(cellValue.length() - max_length));
                    if (cellValue.equals("aaa")) {
                        toReturn.append(String.format("%d%s,", error, repeat));
                    } else {
                        toReturn.append(String.format("%s%s,", cellValue, repeat));
                    }
                }
                error++;
                toReturn.append("},\n");
            }
            return toReturn.append("};").toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "Error " + e.toString();
        }
    }

    void fromCodeToXLS(String txt) {
        try {
            int rowMin0 = 0;
            int colMin0 = 0;
            var workbook = new HSSFWorkbook();
            var sheet = workbook.createSheet("Matriz");

            var lines = Files.readAllLines(Paths.get(txt), StandardCharsets.UTF_8);
            CellStyle styleE = workbook.createCellStyle();
            HSSFFont fontE = workbook.createFont();
            fontE.setBold(true);
            fontE.setColor(HSSFFont.COLOR_RED);
            styleE.setFont(fontE);

            CellStyle style = workbook.createCellStyle();
            HSSFFont font = workbook.createFont();
            font.setBold(true);
            font.setColor(HSSFFont.COLOR_NORMAL);
            style.setFont(font);
            for (var string : lines) {
                var token = string.split("/\\*.*?\\*/|\\{|}|,| ");
                System.out.println(string);
                var row = sheet.createRow(++rowMin0);

                for (var element : token) {
                    element.trim();
                    if (!element.isEmpty() && !element.isBlank()) {
                        var cell = row.createCell(++colMin0);
                        if (Integer.valueOf(element) >= 700) {
                            cell.setCellStyle(styleE);
                            //cell.setCellValue("");
                        } else {
                            cell.setCellStyle(style);
                            cell.setCellValue(element);
                        }
                    }
                }
                colMin0 = 0;
            }
            try (FileOutputStream outputStream = new FileOutputStream("JavaBooks.xls")) {
                workbook.write(outputStream);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    String generateS(int rowMin0, int rowMax0,
                     int errorInicial) {
        return "";
    }

    String generateP(int rowMin0, int rowMax0, int colMin0, int colMax0,
                     int rowMin1, int rowMax1, int colMin1, int colMax1,
                     String glc) {
        try {
            HashMap<String, String> hash_id = new HashMap<>();
            var f = new File(xls);

            getSymbols(hash_id, xls, rowMin0, rowMax0, colMin0, colMax0, rowMin1, rowMax1, colMin1, colMax1);

            var lines = Files.readAllLines(Paths.get(glc), StandardCharsets.UTF_8);

            StringBuilder reversedStack = new StringBuilder();
            var aux_stack = new Stack<String>();
            boolean leftSide;
            for (var line : lines) {
                reversedStack.append("executionStack.append({");
                leftSide = true;
                StringTokenizer tk = new StringTokenizer(
                        line.replace('\n', '°').concat("°"), " "
                );

                while (tk.hasMoreTokens()) {
                    var token = tk.nextToken();
                    if (token.equals("->")) {
                        leftSide = false;
                    } else {
                        if (!leftSide && !token.equals("°")) {
                            aux_stack.push(token);
                        }
                    }
                }

                if (!aux_stack.isEmpty()) {
                    for (int i = 0; i < aux_stack.size() - 1; i++) {
                        reversedStack.append(hash_id.get(aux_stack.pop()));
                        reversedStack.append(", ");
                    }
                    reversedStack.append(hash_id.get(aux_stack.pop()));
                }

                reversedStack.append(")};");
            }
            return reversedStack.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return "Error " + e.toString();
        }
    }

    public static void main(String args[]) {
        FlatLightLaf.install();
        new MainForm();
    }

    private JTextArea textResul;
    private JPanel panel1;
    private JSpinner renMax0;
    private JSpinner renMin0;
    private JSpinner renMax1;
    private JSpinner renMin1;
    private JComboBox cbOpciones;
    private JButton generarButton;
    private JCheckBox cbHeader;
    private JTextField txtGramatica;
    private JButton btnSeleccionarXls;
    private JButton btnSeleccionarGLC;
    private JTextField txtXls;
    private JSpinner header;
    private JSpinner spinnerValorError;
    private JSpinner colMax0;
    private JSpinner colMin0;
    private JSpinner colMax1;
    private JSpinner colMin1;
    private JSpinner sp_max_lenght;
    private JTable table1;
    private JButton loadTableButton;
    private JScrollPane scrollPane;

}
