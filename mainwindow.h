#pragma once

#include <QMainWindow>
#include "QTextCursor"
#include "QRect"
#include "QTextEdit"
#include <highlighter.h>
#include "codeeditor.h"
#include "ui_static_components.h"
#include "config_compiler.h"
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_actionOpen_triggered();

    void on_actionOpen_2_triggered();

    void on_actionSave_triggered();

    void on_actionSave_As_triggered();

    void on_actionClose_triggered();

    void on_actionCheck_Synthax_triggered();

    void on_actionAbout_QT_triggered();

    void on_actionBuild_triggered();

    void on_actionRun_triggered();

    void saveAs();

    void on_actionAbout_ISC_IDE_triggered();

    void on_tabedFiles_tabCloseRequested(int index);

    void on_tabedFiles_currentChanged(int index);

private:
    struct Tabs{
        CodeEditor* codeEditor;
        Highlighter* highligther;
    };
    Tabs *getTab();
    QList<Tabs> readedFileTabs;
    QString nameP;
    QString optimization_level;
    unsigned int processor;
    QString labelCursor;
    Ui::MainWindow *ui;
    config_compiler config;
    void createInstanceCodeEditor(QString id);
};
