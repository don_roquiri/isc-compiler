#pragma once
#include <QString>
#include <QVector>
#include <model_token_semanthic.h>
#include "token.h"
#include "row_cuadruples.h"
/**This contain de cuadruples and the table of symbols!
 *
*/
class CuadrupleMdl :public QVector<RowCuadruple>{
public:
    QHash<QString, ModelTokenSemanthic>* hash_id;
};
